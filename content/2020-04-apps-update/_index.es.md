---
title: Actualización de las Aplicaciones de KDE de abril de 2020

publishDate: 2020-04-23 16:00:00

layout: page # don't translate

summary: "¿Qué ha ocurrido este mes en las Aplicaciones de KDE?"

type: announcement # don't translate
---

¡Ya está aquí un nuevo paquete de aplicaciones de KDE! En este tipo de
lanzamientos puede esperar encontrar más funcionalidades, mejoras de
estabilidad y más herramientas amigables que le ayudarán a trabajar de una
forma más efectiva.

Existen docenas de cambios muy esperados en la mayoría de sus aplicaciones
favoritas. Por ejemplo, Dolphin. Los recursos compartidos Samba de Windows
se pueden descubrir ahora en su totalidad.

En lo relativo a la reproducción de música, el reproductor multimedia Elisa
está añadiendo nuevas funcionalidades a un ritmo acelerado. Este lanzamiento
proporciona una nueva vista «Reproduciendo ahora», cómoda accesibilidad
mediante la bandeja del sistema y una opción para minimizar la lista de
reproducción cuando así lo desee. Gracias al recién añadido modo visual
aleatorio, es mucho más fácil organizar la música en las listas de
reproducción.

Esto es solo un resumen de las novedades de las aplicaciones de KDE de este
mes. Siga leyendo para saber más sobre todo lo que le hemos preparado.

## [Okular](https://okular.kde.org)

Okular es la aplicación de KDE que le permite leer documentos en PDF,
Markdown, libros de cómics y muchos formatos más. En este lanzamiento,
Okular hace gala de algunas mejoras de accesibilidad para los usuarios del
escritorio y de pantallas táctiles. Para los primeros, Okular implementa
ahora un desplazamiento suave, tanto con la rueda del ratón como con el
teclado. Para los usuarios de pantallas táctiles, la última versión de
Okular llega con desplazamiento inercial.

{{<video src="okular.mp4" caption="Desplazamiento suave en Okular. Ilustración: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

Dolphin, el potente gestor de archivos de KDE, contiene nuevas
funcionalidades que le permiten una mejor interacción con los sistemas de
archivos remotos, como recursos compartidos Samba y servidores SSH. Ahora es
posible empezar a ver películas almacenadas en lugares remotos sin necesidad
de descargarlas, ya que se transmiten directamente desde la nube al
reproductor multimedia a través de Dolphin.

De un modo similar, nunca ha sido tan fácil conectar con recursos
compartidos Samba de Windows y aprovecharse de todos los servicios que
ofrecen, ya que ahora se pueden descubrir estos recursos mediante el
protocolo «WS-Discovery» que usan las versiones modernas de Windows. Esto le
permite integrar su equipo Linux de un modo transparente en una red de
Windows.

En cuanto a las novedades del departamento de usabilidad, ahora ya no es necesario usar el ratón para mover el foco desde y hacia el panel de terminal. Ahora se puede usar el acceso rápido de teclado <kbd>Ctrl</kbd> + <kbd>Mayúsculas</kbd> + <kbd>F4</kbd>. Otra funcionalidad muy esperada es la búsqueda de archivos no solo por su nombre o por su contenido, sino también por sus etiquetas.

El soporte de archivos 7Zip está integrado ahora en Dolphin, lo que
significa que puede navegar por estos archivos comprimidos como si se
tratara de carpetas del sistema de archivos. Si tiene instaladas
aplicaciones de búsqueda externas, como KFind, Dolphin le permite acceder
rápidamente a ellas mediante la creación de un enlace.

Por último, una funcionalidad aparentemente insignificante que puede ahorrarle mucho tiempo es la nueva función «Duplicar». Seleccione uno o más archivos o carpetas, pulse <kbd>Ctrl</kbd> + <kbd>D</kbd> (o haga clic con el botón derecho y escoja «Duplicar aquí») y ¡ya está! Aparecerá una copia de cada elemento seleccionado junto a los originales.

![Funcionalidad de duplicación de Dolphin](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

Lokalize es una utilidad que puede usar para traducir archivos gettext y
xliff. Ahora permite el uso de correcciones gramaticales con Language-Tool,
que ayuda a los traductores resaltando los errores gramaticales existentes
en el texto.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

KMail es el cliente de correo electrónico más popular de KDE y forma parte
de Kontact, un paquete de programas que le ayudan a gestionar correo
electrónico, contactos y tareas.

Son muchas las mejoras incluidas en este nuevo lanzamiento. Para empezar,
los mensajes de correo electrónico se pueden exportar fácilmente a PDF,
mientras que los mensajes formateados con Markdown se muestran ahora
mejor. En lo relativo a seguridad también se ha producido alguna mejora. En
concreto, KMail muestra una advertencia cuando el compositor de mensajes se
abre al pulsar un enlace que le solicita adjuntar un archivo.

KMail es el cliente de correo electrónico más popular de KDE y forma parte
de Kontact, un paquete de programas que le ayudan a gestionar correo
electrónico, contactos y tareas. Son muchas las mejoras incluidas en este
nuevo lanzamiento. Para empezar, los mensajes de correo electrónico se
pueden exportar fácilmente a PDF, mientras que los mensajes formateados con
Markdown se muestran ahora mejor. En lo relativo a seguridad también se ha
producido alguna mejora. En concreto, KMail muestra una advertencia cuando
el compositor de mensajes se abre al pulsar un enlace que le solicita
adjuntar un archivo.

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

Konsole es un emulador de terminal que se usa para ejecutar instrucciones, procesos y programas desde la línea de órdenes. La última versión de Konsole le permite usar la tecla <kbd>Alt</kbd> + una tecla de número para saltar directamente a cualquiera de las primeras 9 pestañas.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

Gwenview es el visor de imágenes favorito de KDE. En esta versión, los
desarrolladores han resuelto dos problemas importantes. La aplicación ya no
se bloquea durante el inicio cuando el portapapeles del sistema contiene
texto de KDE Connect. También se ha corregido el acceso a lugares remotos
para importar o exportar fotos.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

Elisa es un sencillo y popular reproductor de música. Este lanzamiento
presenta la vista «Reproduciendo ahora», que lo hace más atractivo.

Pero no todo tiene que ver con el aspecto visual. Elisa también es más
funcional, ya que ahora se puede acceder a la aplicación mediante la bandeja
del sistema y minimizar la lista de reproducción cuando lo desee. Esto
significa que puede seguir oyendo su música favorita incluso cuando la
ventana principal de Elisa esté cerrada. El nuevo modo aleatorio visual
facilita la organización de la música en las listas de reproducción y le
muestra la próxima canción que se va a reproducir.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

Kdenlive es la aplicación de edición de vídeo avanzada de KDE. En esta
nueva versión se agiliza la edición de vídeo gracias a que se puede
configurar la resolución de la vista previa. La vista previa también mejora
la visualización multipista, haciendo que sea más fácil seleccionar pistas
de entre todas las que esté editando al mismo tiempo.

En la línea de tiempo, el equipo ha mejorado el ajuste de los clips de vídeo
haciendo que se solo se ajusten en las pistas activas. También han corregido
la función de ajuste de grupo y han añadido un acceso rápido para evitar el
ajuste al mover. Otra cosa que se puede hacer ahora es arrastrar archivos
(vídeos, música e imágenes) directamente desde el explorador de archivos y
soltarlos sobre la línea de tiempo. La nueva funcionalidad de «ampliación en
los fotogramas clave» le ayudará a realizar un trabajo más preciso. Los
editores de vídeo y usuarios profesionales acostumbrados a usar otras
herramientas de edición apreciarán sin duda la funcionalidad de importar y
exportar «OpenTimelineIO».

Este lanzamiento de Kdenlive también contiene numerosas correcciones de
errores, como los de seguimiento del movimiento, que no funcionaba en
algunos sistemas. Ahora permite añadir un filtro de compensación de
lanzamiento, así como añadir «mover y cambiar de tamaño» a la forma
«Rotoscopia». También se han corregido un bloqueo el asistente de DVD y un
error que se producía en el cambio del volumen de la grabación desde el
mezclador. El equipo de Kdenlive también ha añadido funcionalidad de
etiquetado, puntuación y filtrado a la bandeja del proyecto.

La nueva versión de Kdenlive para Windows también contiene correcciones de
errores para los diálogos de la línea de tiempo, que anteriormente no
aceptaba entrada del teclado. De forma adicional, ahora se puede seleccionar
el motor de sonido en Windows.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

Yakuake es una ventana de terminal que puede bajar desde la parte superior del escritorio Plasma pulsando <kbd>F12</kbd>. Resulta práctica para ejecutar instrucciones rápidas en la línea de órdenes y luego hacerla desaparecer.

Cuando abra nuevas pestañas o divida paneles en esta versión de Yakuake,
ahora podrá iniciarlas en el mismo directorio que la pestaña o división
actual. También puede cambiar el tamaño de la ventana de Yakuake en la
vertical arrastrando su barra inferior.

## Preferencias del sistema

La página de integración de las cuentas en línea de las Preferencias del
sistema le permite configurar cosas como su cuenta de Google o el acceso a
su cuenta de Nextcloud. La página se ha rediseñado completamente y ahora
presenta un aspecto limpio y moderno con una funcionalidad mucho más fiable.

## [KDE Connect](https://kdeconnect.kde.org/)

KDE Connect es una aplicación que le ayuda a integrar su teléfono móvil con
su escritorio y viceversa. Una de las funcionalidades más recientes que
acompañan a este lanzamiento es la posibilidad de iniciar nuevas
conversaciones con la aplicación de SMS.

Otros cambios que también incluye son la posibilidad de mostrar los
reproductores multimedia remotos en la miniaplicación de reproducción
multimedia, un nuevo conjunto de iconos de estado para las conexiones y un
manejo de notificaciones de llamadas mejorado.

En la sección de corrección de errores, se ha corregido el error que hacía
que al compartir un archivo se bloqueara la aplicación que lo
llamaba. También se han resuelto los problemas de las transferencias de
archivos realmente grandes.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

Spectacle, el software de captura de pantallas de KDE, incorpora
funcionalidades de usabilidad y resuelve algunas asperezas. Una de ellas
impedía copiar una captura de pantalla en el portapapeles al lanzar
Spectacle desde la línea de órdenes con la opción «--nonotify». Aunque este
podía haber sido un problema especializado, el asunto es que ahora se puede
hacer exactamente eso. Del mismo modo, las versiones anteriores tenían
problemas al tratar de arrastrar una captura de pantalla desde Spectacle
hasta el patrón de nombre de archivo por omisión que incluyera
subcarpetas. Todos estos problemas han sido resueltos.

Otra útil mejora de la nueva versión de Spectacle llega en la forma de
botones «Valores predeterminados» y «Revertir». Los puede usar para
retroceder a una configuración anterior, e incluso eliminar todas las
configuraciones personalizadas anteriores y restaurar todo a su valor
predeterminado.

Spectacle no solo recordará la confirmación predeterminada, ya que además
recordará el área de la captura de pantalla que se realizó en último
lugar. Esto le permite ser consistente, haciendo que sea más fácil crear
capturas de la misma forma y tamaño que las anteriores.

## [Krita](https://krita.org/en/) 4.2.9

Existe cierto número de actualizaciones técnicas entre bastidores en esta
versión, aunque también nuevas funcionalidades, como las dos nuevas opciones
que se han añadido al pincel de manchas de colores («Aerógrafo» y «Ritmo del
aerógrafo»). La mezcla de colores también contiene una nueva preferencia que
permite usar los distintos sensores para aplanar la forma del pincel. La
nueva función «dividir la capa en máscara de selección» le permite crear una
nueva capa para cada color de la capa activa. También proporciona contornos
de pincel más suaves que no parpadean al situar el cursor sobre el lienzo.

Existe cierto número de actualizaciones técnicas entre bastidores en esta
versión, aunque también contornos de pincel más suaves, que no parpadean al
situar el cursor sobre el lienzo. El «Aerógrafo» y el «Ritmo del aerógrafo»
se han añadido al pincel de mezcla de colores. El pincel de mezcla de
colores también contiene una nueva preferencia que permite usar los
distintos sensores para aplanar la forma del pincel. La nueva función
«dividir la capa en máscara de selección» le permite crear una nueva capa
para cada color de la capa activa.

<iframe src=https://www.youtube.com/embed/fyc8-qgxAww width=560 height=315 frameborder=0 allowfullscreen=allowfullscreen></iframe>

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

La versión 3.0.4 de Smb4K corrige varios problemas y bloqueos, especialmente
en el montador. También hace que se vuelva a poder compilar Smb4K con Qt
versión 5.9 o inferior.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Esta versión mayor del complemento que integra Google Drive con las
aplicaciones de KDE añade la implementación de la funcionalidad de unidades
compartidas de Google Drive.

También incluye una nueva acción «Copiar URL de Google en el portapapeles»
en el menú de contexto de Dolphin.
