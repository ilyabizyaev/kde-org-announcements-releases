---
title: Atualização de abril de 2020 dos aplicativos da KDE

publishDate: 2020-04-23 16:00:00

layout: page # don't translate

summary: "O que aconteceu com o KDE Applications este mês"

type: announcement # don't translate
---

A nova atualização dos aplicativos da KDE chegou! Nestes lançamentos, você
verá novas funcionalidades, melhorias de estabilidade e mais ferramentas de
uso amigável para te ajudar a trabalhar de modo mais eficaz.

Você já pode se animar com as dezenas de alterações na maioria dos seus
aplicativos favoritos. No caso do Dolphin, por exemplo, os compartilhamentos
do tipo Windows Samba agora são navegáveis.

Quanto a tocar suas músicas: o reprodutor de música Elisa tem ganho
funcionalidades a passos largos. Este lançamento vem com o novo modo
"Reproduzindo agora", acesso fácil na área de notificação e uma opção para
minimizar a lista de reprodução quando você quiser. Graças ao modo
embaralhar adicionado recentemente agora se tornou mais fácil reorganizar a
sua música nas listas de reprodução.

Estes são apenas alguns dos enfoques do que há de novo nos aplicativos da
KDE deste mês. Leia mais para ficar por dentro do que preparamos para você.

## [Okular](https://okular.kde.org)

O Okular é o aplicativo da KDE que te permite ler PDFs, documentos Markdown,
quadrinhos e muitos outros formatos de documento. Neste lançamento, o Okular
recebeu algumas melhorias de acessibilidade usuários de desktop e telas de
toque. No caso do primeiro, o Okular agora implementou rolagem suave tanto
usando a roda do mouse quanto o teclado. Para quem usa tela de toque, a
última versão do Okular vem com rolagem com efeito de inércia.

{{<video src="okular.mp4" caption="Rolagem suave no Okular. Illustration: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

O poderoso gerenciador de arquivos da KDE, o Dolphin, agora tem novas
funcionalidades, permitindo a você interagir melhor com sistemas de arquivos
remotos como compartilhamentos Samba e servidores SSH. Agora você pode
assistir filmes salvos em lugares remotos sem ter que baixá-los,
visualizando eles por streaming diretamente da nuvem para seu reprodutor por
meio do Dolphin.

De maneira semelhante, conectar aos compartilhamentos Samba e aproveitar
seus serviços nunca foi mais fácil. Agora, compartilhamentos Samba são
navegáveis por meio do protocolo WS-Discovery usado em versões modernas do
Windows. Isso permite que você integre sua máquina com Linux a uma rede
Windows facilmente.

Também há notícias de usabilidade: você não precisa mais usar seu mouse para transferir o foco entre a navegação e o painel de terminal. Agora você pode simplesmente usar o atalho de teclado <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F4</kbd>. Outra funcionalidade há muito desejada é a pesquisa de arquivos não só por nome de arquivo ou conteúdo mas também por tags.

O suporte a arquivos 7Zip agora já vem com o Dolphin, deste modo você poderá
navegar diretamente pelos arquivos compactados como se fossem pastas no
sistema de arquivos. Caso você instale aplicativos de pesquisa externos como
o KFind, o Dolphin agora te dará acesso rápido a eles criando um link.

Por fim, uma funcionalidade aparentemente pequena que pode poupar bastante tempo é a nova funcionalidade de "Duplicar". Selecione um ou mais arquivos ou pastas, aperte <kbd>Ctrl</kbd> + <kbd>D</kbd> (ou clique com o botão direito e selecione "Duplicar aqui") e pronto, uma cópia de cada item selecionada irá aparecer ao lado dos originais.

![Funcionalidade Duplicar do Dolphin](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

O Lokalize é um programa para traduzir arquivos gettext e xliff. Ele agora
suporta correção gramatical Language-Tool, o que ajuda tradutores a demarcar
erros gramaticais no texto.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

O KMail é o cliente de email mais popular da KDE e parte do Kontact, uma
suíte de programas que permite a você gerenciar seus emails, contatos e
tarefas.

Várias melhorias foram incluídas neste novo lançamento. Para começar,
mensagens de email agora podem ser facilmente exportadas para PDF e
messagens formatadas em Markdown agora são renderizadas melhor. O aspecto de
segurança também teve suas melhoras. O KMail agora exibe um aviso caso o
compositor de mensagens abra após clicar em um link que pede a você anexar
um arquivo.

O KMail é o cliente de email mais popular da KDE e parte do Kontact, uma
suíte de programas que permite a você gerenciar seus emails, contatos e
tarefas. Várias melhorias foram incluídas neste novo lançamento. Para
começar, mensagens de email agora podem ser facilmente exportadas para PDF e
messagens formatadas em Markdown agora são renderizadas melhor. O aspecto de
segurança também teve suas melhoras. O KMail agora exibe um aviso caso o
compositor de mensagens abra após clicar em um link que pede a você anexar
um arquivo.

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

O Konsole é o emulador de terminal que te permite executar instruções de linha de comando, processos e programas. A última versão do Konsole permite que você use a tecla <kbd>Alt</kbd> + número para pular diretamente para quaisquer das 9 abas.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

O Gwenview é o visualizador de imagens favorito da KDE e nesta versão os
desenvolvedores consertaram dois erros sérios. O aplicativo não trava mais
ao ser executado quando a área de transferência do sistema contém texto do
KDE Connect, e a importação de fotos de e para locais remotos foi arrumada.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

O Elisa é um reprodutor de música simples e popular, e este lançamento
inclui o modo "Reproduzindo agora", o que torna ele mais atraente.

Não é só melhoria na aparência, no entanto. O Elisa está mais funcional
agora que é possível acessar pela área de notificação e minimizar a lista de
reprodução quando quiser. Isso significa que você pode continuar ouvindo sua
música favoita mesmo quando a janela do Elisa estiver fechada. O novo modo
visual de embaralhar torna mais fácil reorganizar sua música nas listas de
reprodução, exibindo qual música irá tocar novamente.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

O Kdenlive é o aplicativo de edição de vídeo avançado da KDE, e a nova
versão vem com edição mais rápida graças à sua resolução de pré-visualização
configurável. A pré-visualização também melhora a visualização multifaixas,
facilitando selecionar faixas daquelas que você estiver editando, todas ao
mesmo tempo.

Já quanto à linha do tempo, a equipe melhorou o encaixe de clipes permitindo
apenas encaixes nas faixas ativas. Eles também consertaram a função de
encaixe em grupo e incluíram um atalho para ignorar o encaixe ao
mover. Outra coisa que é possível agora é arrastar diretamente seus arquivos
(vídeos, músicas e imagens) do seu gerenciador de arquivos para a linha do
tempo. A nova função "zoom nos quadros-chave" permite trabalhar ainda mais
precisamente. Editores de vídeo profissionais e usuários advindos de outras
ferramentas de edição irão sem dúvida apreciar a função de importar/exportar
OpenTimelineIO.

Este lançamento do Kdenlive também vem com várias correções de erros, como
os do Rastreador de movimento que estava quebrado em alguns sistemas. O
Kdenlive agora permite adicionar filtro de compensação de frequência assim
como o "Mover e redimensionar" a uma forma de giroscópio. O crash do
assistente de DVD e um erro que assolava a alteração de volume de gravação
do mixer também foram resolvidos. A equipe do Kdenlive também incluiu as
funções de tags, avaliações e filtragem ao Arquivos do projeto.

A nova versão do Kdenlive para Windows também inclui correções nas janelas
da linha do tempo que antes não aceitava entrada do teclado. Além disso,
agora você também pode selecionar a infraestrutura de áudio no Windows.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

O Yakuake é uma janela de terminal que é puxada para baixo do topo da área de trabalho Plasma pressionando <kbd>F12</kbd>. Ele é bem conveniente para executar instruções rápidas na linha de comando e remover da frente da interface.

Agora, quando você abre novas abas ou painéis divididos no Yakuake, você
pode iniciá-los ma mesma pasta da aba/painel atual. Você também pode
redimensionar a janela do Yakuake verticalmente arrastando sua barra
inferior.

## Configurações do sistema

A página de integração de Contas online nas Configurações de sistema permite
que você configure coisas como sua conta da Google ou definir acesso à sua
conta Nextcloud. A página foi completamente renovada e inclui um visual
moderno e limpo com funções muito mais robustas.

## [KDE Connect](https://kdeconnect.kde.org/)

O KDE Connect é um aplicativo que te ajuda a integrar seu celular com sua
área de trabalho e vice-versa. Uma de suas funcionalidades mais recentes
neste lançamento é a capacidade de começar novas conversas com o app de SMS.

Outras mudanças incluem reprodutores de mídia externos aparecendo no applet
reprodutor de mídia, um novo conjunto de ícones de status de conexão, e
melhorias no método de notificação de chamadas.

Quanto a correções de erros, um que fazia o compartilhamento de arquivos
bloquear o aplicativo de chamadas foi resolvido e outros problemas com a
transferência de arquivos muito grandes foram resolvidos.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

O Spectacle, o software de captura de tela da KDE, incluiu funções de
usabilidade e teve alguns erros menores arrumados. Um deles impedia a cópia
de uma captura de tela para a área de transferência ao abrir o Spectacle
pela linha de comando com a opção --nonotify. Embora tenha sido um problema
nicho, a questão é que agora você pode fazer isso. Similarmente, versões
antigas apresentavam problemas ao tentar arrastar uma captura do Spectacle
para o padrão de nome de arquivo padrão que incluía subpastas. Estes
problemas foram resolvidos.

Outra melhoria útil na nova versão do Spectacle vem na forma dos botões
"Padrão"  e "Reverter". Você pode usá-los para voltar a uma configuração
anterior ou mesmo remover todas e quaisquer customizações e restaurar para
os valores padrão.

O Spectacle não somente irá lembrar sua configuração padrão, mas também irá
lembrar a última área usada na captura de área. Isso ajuda a manter a
consistência ao criar capturas da mesma forma e tamanho das anteriores.

## [Krita](https://krita.org/en/) 4.2.9

Houve um número de atualizações técnicas por trás dos bastidores neste
lançamento, mas também houveram novas funcionalidades como as duas novas
opções adicionadas ao pincel Borrão de cores — "Pincel de spray" e "Taxa de
spray". O pincel Borrão de cores também ganhou uma configuração nova de
Proporção que te permite usar diferentes sensores para tornar a forma do
pincel mais plano. A função "Camada dividida na máscara de seleção" permite
que você crie uma nova camada para cada cor na camada ativa. Também há
contornos de pincel de pintura mais macios que não se mexe ao passar por
cima da área de desenho.

Houve um número de atualizações técnicas por trás dos bastidores neste
lançamento, mas também contornos de pincel de pintura que não se mexe ao
passar por cima da área de desenho. O pincel Borrão de cores recebeu as
opções "Pincel de spray" e "Taxa de spray" além de uma configuração nova de
Proporção que te permite usar diferentes sensores para tornar a forma do
pincel mais plano. A função "Camada dividida na máscara de seleção" permite
que você crie uma nova camada para cada cor na camada ativa.

<iframe src=https://www.youtube.com/embed/fyc8-qgxAww width=560 height=315 frameborder=0 allowfullscreen=allowfullscreen></iframe>

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

O lançamento do Smb4K 3.0.4 corrige vários erros e crashes, em particular no
montador. O Smb4K também se tornou novamente compilável com Qt versão 5.9 ou
anterior.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Este importante lançamento do plugin que integra o Google Drive aos
aplicativos da KDE inclui suporte à função Drives compartilhados do Google
Drive.

Também está inclusa a nova ação de menu de contexto do Dolphin "Copiar URL
da Google para a Área de transferência".
