---
title: Actualização de Abril de 2020 do KDE

publishDate: 2020-04-23 16:00:00

layout: page # don't translate

summary: "O que se Passou nas Aplicações do KDE Neste Mês"

type: announcement # don't translate
---

Está aí um novo grupo de aplicações do KDE: Nessas versões, poderá esperar
encontrar mais funcionalidades, melhorias de estabilidade e ferramentas mais
amigáveis que o irão ajudar a trabalhar de forma mais efectiva.

Existem dezenas de alterações a investigar na maioria das suas aplicações
favoritas, como por exemplo o Dolphin. As partilhas de Samba no Windows
agora são completamente detectadas.

No tópico de reprodução de música: o leitor de música Elisa tem adicionado
funcionalidades atrás de funcionalidades. Esta versão traz uma nova área
"Agora a Tocar", uma acessibilidade simples através da bandeja do sistema e
uma opção para minimizar a lista de reprodução sempre que quiser. Graças ao
modo aleatório recentemente adicionado, é muito mais simples reorganizar a
sua música nas listas de reprodução.

Estes são apenas os destaque do que há de novo nas aplicações do KDE neste
mês. Continue a ler para descobrir tudo o que preparámos para si.

## [Okular](https://okular.kde.org)

O Okular é uma aplicação do KDE que lhe permite ler PDF's, documentos de
Markdown, bandas desenhadas e muitos outros formatos de documentos. Nesta
versão, o Okular recebeu algumas melhorias de acessibilidade no ambiente de
trabalho e nos ecrãs tácteis. No primeiro caso, o Okular agora implementa um
deslocamento suave quando está a usar a roda do rato e o teclado. Para os
utilizadores de ecrãs tácteis, a última versão do Okular vem com
deslocamento por inércia.

{{<video src="okular.mp4" caption="Deslocamento suave do Okular. Ilustração: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

O poderoso gestor de ficheiros do KDE Dolphin adiciona novas
funcionalidades, permitindo-lhe interagir melhor com os sistemas de
ficheiros remotos, como as partilhas de Samba e os servidores de SSH. Agora
poderá começar a ver os filmes gravados nos servidores remotos sem ter de os
transferir, transmitindo-os directamente a partir da 'cloud' para o seu
leitor através do Dolphin.

Numa abordagem semelhante, a ligação às partilhas de Samba do Windows e o
partido que se tira de todos os serviços que elas oferecem nunca foi mais
fácil, dado que as partilhas de Samba podem agora ser descobertas com o
protocolo WS-Discovery que é usado nas versões recentes do Windows. Isto
permite-lhe integrar a sua máquina Linux de forma transparente com uma rede
de Windows.

No capítulo das notícias do departamento de usabilidade: não precisa mais do seu rato para passar o foco de e para o painel do terminal. Agora poderá simplesmente usar a combinação de teclas <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F4</kbd>. Outra funcionalidade há muito tempo aguardada é a pesquisa dos ficheiros não só pelo seu nome ou conteúdo, mas também pelas suas marcas.

O suporte para os ficheiros 7Zip está agora incorporado no Dolphin, o que
significa que poderá navegar nesses ficheiros comprimidos como se fossem
pastas do sistema de ficheiros. Quando instalar as aplicações de pesquisa
externas como o KFind, o Dolphin permite-lhe aceder rapidamente aos mesmos
criando uma ligação.

Finalmente, uma funcionalidade relativamente pequena que poderá poupar bastante tempo é a função "Duplicar". Seleccione um ou mais ficheiros ou pastas, carregue em <kbd>Ctrl</kbd> + <kbd>D</kbd> (ou carregue com o botão direito e escolha "Duplicar Aqui") e pumba! Irá aparecer uma cópia de cada um dos itens seleccionados imediatamente ao lado dos originais.

![Funcionalidade de duplicados do Dolphin](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

O Lokalize é um utilitário para traduzir ficheiros do Gettext e XLIFF. Agora
suporta a correcção gramatical do Language-Tool, o que ajuda os tradutores a
realçar os erros gramaticais no texto.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

O KMail é o cliente de e-mail mais conhecido do KDE e faz parte do Kontact,
um pacote de programas que o ajuda a gerir os seus e-mails, contactos e
tarefas.

Foram incluídas algumas melhorias nesta nova versão. Para começar, as
mensagens de e-mail podem ser agora exportadas rapidamente parra PDF, e as
mensagens formatadas em Markdown são melhor apresentadas. O aspecto de
segurança também recebeu alguma atenção. Em específico, o KMail mostra um
aviso quando o compositor de mensagens abre após carregar numa ligação que
lhe pede para anexar um ficheiro.

O KMail é o cliente de e-mail mais conhecido do KDE e faz parte do Kontact,
um pacote de programas que o ajudam a gerir os seus e-mails, contactos e
tarefas. Foram incluídas algumas melhorias nesta nova versão. Para começar,
as mensagens de e-mail podem ser agora exportadas rapidamente parra PDF, e
as mensagens formatadas em Markdown são melhor apresentadas. O aspecto de
segurança também recebeu alguma atenção. Em específico, o KMail mostra um
aviso quando o compositor de mensagens abre após carregar numa ligação que
lhe pede para anexar um ficheiro.

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

O Konsole é um emulador de terminal que pode usar para executar instruções da linha de comandos, processos e programas. A última versão do Konsole permite-lhe usar a tecla <kbd>Alt</kbd> + uma tecla de número para ir directamente para qualquer uma das primeiras 9 páginas.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

O Gwenview é o visualizador de imagens favorito do KDE e, nesta versão, os
programadores resolveram dois problemas importantes. A aplicação não mais
fica pendurada no lançamento quando a área de transferência do sistema
contém texto do KDE Connect e a importação de fotografias de e para
localizações remotas foi corrigida.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

O Elisa é um leitor de música simples e popular, sendo que esta versão
oferece a área "Agora a Tocar", que o torna mais atractivo.

Contudo, não é só uma questão de visual. O Elisa também está mais funcional,
dado que poderá aceder a ele através da bandeja do sistema e minimizar a
lista de reprodução sempre que entender. Isto significa que poderá continuar
a ouvir a sua música favorita mesmo quando a janela principal do Elisa está
fechada. O novo modo visual aleatório torna mais simples reorganizar a sua
música nas listas de reprodução e mostra-lhe qual a música que será tocada a
seguir.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

O Kdenlive é a aplicação de edição avançada de vídeo do KDE, sendo que a
nova versão torna a edição mais rápida graças á sua resolução de antevisão
configurável. A antevisão também melhora a visualização multi-faixas,
tornando mais simples a selecção das faixas a partir das que está a editar,
tudo ao mesmo tempo.

No que respeita à linha temporal, a equipa melhorou o ajuste dos 'clips',
ajustando-se apenas nas faixas activas. Também corrigiram a funcionalidade
de ajuste ao grupo e adicionaram um atalho para desactivar o ajuste em caso
de movimento. Outra coisa que agora poderá fazer é largar directamente os
seus ficheiros (vídeos, músicas e imagens) do seu explorador de ficheiros
para a linha temporal. A nova funcionalidade de "ampliação nas
imagens-chave" ajudá-lo-á a criar um trabalho mais preciso. Os editores de
vídeo e utilizadores profissionais que vêm de outras ferramentas de edição
irão sem dúvida apreciar a funcionalidade de importação/exportação para
OpenTimelineIO.

Esta versão do Kdenlive também vem com muitas correcções de erro, como as do
Detector de Movimento, que estava com problemas em alguns sistemas. Agora
permite-lhe adicionar um filtro de Compensação de Timbre, assim como a
adição da opção para "Mover e dimensionar" numa forma do Rotoscoping. O
estoiro do Assistente de DVD's e um erro que prejudicava a alteração do
volume de gravação na mesa de mistura. A equipa do Kdenlive também adicionou
a marcação, classificação e a funcionalidade de filtragem no grupo do
projecto.

A nova versão para Windows do Kdenlive também vem com correcções nas janelas
da linha temporal, as quais antigamente não aceitavam nenhuma interacção com
o teclado. Para além disso agora poderá seleccionar a infra-estrutura de
áudio do Windows.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

O Yakuake é uma janela de terminal que poderá puxar do topo do ecrã do Plasma ao carregar em <kbd>F12</kbd>. É útil para executar rapidamente instruções na linha de comandos, tirando-a depois fora do caminho.

Quando abrir páginas novas ou dividir as áreas nesta versão do Yakuake,
podê-las-á iniciar na mesma pasta que a página/divisão actual. Também poderá
dimensionar a janela do Yakuake na vertical, arrastando a sua barra
inferior.

## Configuração do Sistema

A página de integração de contas 'online' na Configuração do Sistema
permite-lhe configurar algumas coisas, como a sua conta do Google ou o
acesso à sua conta do Nextcloud. A página foi completamente remodelada,
oferecendo agora um visual moderno e limpo com uma funcionalidade muito mais
fiável.

## [KDE Connect](https://kdeconnect.kde.org/)

O KDE Connect é uma aplicação que o ajuda a integrar o seu telefone com o
seu ambiente de trabalho e vice-versa. Uma das funcionalidades mais recentes
nesta versão é a capacidade de iniciar conversas com a aplicação de SMS's.

Outras alterações incluem o acesso aos leitores multimédia remotos na
'applet' do leitor multimédia, um novo conjunto de ícones de estado da
ligação e o tratamento melhorado das notificações de chamadas.

No capítulo das correcções de erros, o erro que fazia com que a partilha de
ficheiros bloqueasse a aplicação que os invocava foi resolvida, assim como
os problemas na transferência de ficheiros realmente grandes foi resolvida.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

O Spectacle, a aplicação de capturas do ecrã do KDE, adicionou algumas
funcionalidades de usabilidade e resolveu alguns pequenos problemas. Um
deles impedia-o de copiar uma fotografia para a área de transferência quando
abria o Spectacle a partir da linha de comandos com a opção
--nonotify. Ainda que este possa ter sido um problema específico, o
importante é que agora já pode fazê-lo. Da mesma forma, as versões mais
antigas tinham problemas quando tentava arrastar uma fotografia do Spectacle
para o padrão de nomes de ficheiros predefinido, caso incluísse
sub-pastas. Estes problemas foram agora resolvidos.

Outra melhoria útil na nova versão do Spectacle vem na forma dos botões
"Predefinições" e "Reverter". Poderá usar os mesmos para recuar para uma
configuração anterior e repor tudo com os seus valores predefinidos.

O Spectacle não só irá recordar a sua configuração predefinida como também a
última área de captura usada. Isto ajuda-o a manter-se consistente,
simplificando a criação de fotografias da mesma forma e tamanho que as
anteriores.

## [Krita](https://krita.org/en/) 4.2.9

Ocorreram algumas actualizações técnicas nos bastidores com esta versão, mas
também algumas funcionalidades novas como as duas novas opções no pincel de
Manchas de Cores - o “Spray” e a “Taxa do Spray.”. As Manchas de Cores agora
também ganharam uma nova opção Proporção que lhe permite usar os diferentes
sensores para tornar mais plana a forma do pincel. Uma nova funcionalidade
"Dividir a Camada para uma Máscara de Selecção" que lhe permite criar uma
nova camada para cada cor na camada activa. Para além disso, os contornos
dos pincéis de pintura já não piscam quando passa o cursor sobre a área de
desenho.

Ocorreram algumas actualizações técnicas nos bastidores com esta versão, mas
também os contornos dos pincéis de pintura já não piscam quando passa o
cursor sobre a área de desenho. Foram adicionadas novas opções no pincel de
Manchas de Cores - o “Spray” e a “Taxa do Spray.”, assim como uma nova opção
Proporção que lhe permite usar os diferentes sensores para tornar mais plana
a forma do pincel. Uma nova funcionalidade Dividir a Camada para uma Máscara
de Selecção permite-lhe criar uma nova camada para cada cor na camada
activa.

<iframe src=https://www.youtube.com/embed/fyc8-qgxAww width=560 height=315 frameborder=0 allowfullscreen=allowfullscreen></iframe>

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

A versão 3.0.4 do Smb4K corrige diversos problemas e estoiros, em especial
na montagem. Também possibilita ao Smb4K ser compilado de novo com a versão
5.9 ou inferiores do Qt.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Esta versão importante do 'plugin' que integra o Google Drive com as
aplicações do KDE adiciona o suporte para a funcionalidade de Unidades
Partilhadas do Google Drive.

Também inclui uma nova acção "Copiar o URL do Google para a área de
transferência" no menu de contexto do Dolphin.
