---
title: Actualització de les aplicacions del KDE d'abril de 2020

publishDate: 2020-04-23 16:00:00

layout: page # don't translate

summary: "Què ha passat a les aplicacions del KDE aquest mes"

type: announcement # don't translate
---

Ja hi ha aquí un paquet nou de les aplicacions del KDE! En aquests
llançaments podeu esperar trobar més funcionalitats, millores d'estabilitat,
i eines més fàcils d'usar que us ajudaran a treballar amb més efectivitat.

Hi ha dotzenes de canvis que us esperen a la majoria de les aplicacions
preferides. Prenem com a exemple el Dolphin. Les comparticions Samba del
Windows ara es poden descobrir completament.

Amb el tema de reproduir música: el reproductor musical Elisa ha afegit
funcionalitats a passos de gegant. Aquest llançament aporta una vista nova
d'«Ara s'està reproduint», fàcilment accessible a través des de la safata
del sistema, i una opció per a minimitzar la llista de reproducció a on es
vulgui. Gràcies al mode visual de barreja afegit recentment, és molt més
fàcil reorganitzar la música a les llistes de reproducció.

Hi ha els punts més destacats de les novetats a les aplicacions del KDE
d'aquest mes. Llegiu-ho per trobar tot el que s'ha preparat per vós.

## [Okular](https://okular.kde.org)

L'Okular és l'aplicació del KDE que permet llegir PDF, documents Markdown,
llibres de còmic, i molts altres formats de documents. En aquest llançament
l'Okular ha rebut millores d'accessibilitat per als usuaris de l'escriptori
i també de pantalles tàctils. Per als primers, l'Okular ara implementa el
desplaçament suau tant quan s'usa la roda del ratolí com el teclat. Per als
usuaris de pantalles tàctils, la darrera versió de l'Okular ve amb el
desplaçament inercial.

{{<video src="okular.mp4" caption="Okular smooth scrooling. Illustration: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

El potent gestor de fitxers Dolphin del KDE afegeix funcionalitats noves,
permetent interactuar millor amb els sistemes de fitxers remotes com les
comparticions Samba i els servidors SSH. Ara podeu començar a mirar
pel·lícules emmagatzemades a llocs remots sense que calgui baixar-les, i en
el seu lloc podeu enviar la transmissió directament des del núvol al
reproductor mitjançant el Dolphin.

D'una manera semblant, mai ha estat tant fàcil connectar amb comparticions
Samba del Windows i aprofitar tots els serveis que ofereixen, ja que les
comparticions Samba ara es poden descobrir via el protocol WS-Discovery usat
per les versions modernes del Windows. Això permet integrar sense costures
l'equip amb Linux a una xarxa Windows.

Pel que fa a les notícies dels departament d'usabilitat: ja no cal el ratolí per moure el focus a i des del plafó de terminal. Ara podeu usar la drecera de teclat <kbd>Ctrl</kbd> + <kbd>Majúscules</kbd> + <kbd>F4</kbd>. Una altra funcionalitat llargament esperada és la cerca de fitxers no només pels seus noms o contingut, sinó també per les seves etiquetes.

Ara s'han implementat els fitxers 7Zip dins el Dolphin, i això vol dir que
es pot navegar per aquests fitxers comprimits com si fossin carpetes del
sistema de fitxers. Quan s'instal·lin aplicacions externes de cerca com el
KFind, el Dolphin permet accedir-hi ràpidament creant un enllaç.

Finalment, una funcionalitat aparentment petita que pot ser una gran estalviadora de temps és la funció «Duplica». Seleccioneu un o més fitxers o carpetes, premeu <kbd>Ctrl</kbd> + <kbd>D</kbd> (o clic dret i trieu «Duplica aquí») i - bum! Apareix una còpia de cada element seleccionat al costat dels originals.

![Funcionalitat de duplicat del Dolphin](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

El Lokalize és una utilitat que es pot usar per a traduir fitxers «gettext»
i «xliff». Ara admet la correcció gramatical del Language-Tool, que ajuda
als traductors ressaltant els errors gramaticals del text.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

El KMail és el client de correu electrònic més popular i és part del
Kontact, un paquet de programes que ajuden a gestionar els correus
electrònics, els contactes, i les tasques.

S'han inclòs diverses millores en aquest llançament nou. Per començar, ara
els missatges de correu es poden exportar fàcilment a PDF, i els missatges
amb format Markdown es mostren millor. L'aspecte de la seguretat també ha
rebut una atenció especial. En concret, el KMail mostra un avís quan s'obre
l'editor de missatge després de fer clic a un enllaç que demana adjunta un
fitxer.

El KMail és el client de correu electrònic més popular i és part del
Kontact, un paquet de programes que ajuden a gestionar els correus
electrònics, els contactes, i les tasques. S'han inclòs diverses millores en
aquest llançament nou. Per començar, ara els missatges de correu es poden
exportar fàcilment a PDF, i els missatges amb format Markdown es mostren
millor. L'aspecte de la seguretat també ha rebut una atenció especial. En
concret, el KMail mostra un avís quan s'obre l'editor de missatge després de
fer clic a un enllaç que demana adjunta un fitxer.

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

El Konsole és un emulador de terminal que es pot usar per executar instruccions de la línia d'ordres, processos, i programes. La darrera versió del Konsole permet usar la tecla <kbd>Alt</kbd> + una tecla numèrica per a saltar directament a qualsevol de les 9 primeres pestanyes.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

El Gwenview és el visor d'imatges preferit del KDE, i en aquesta versió els
desenvolupadors han resolt dos problemes importants. L'aplicació ja no es
penja en llançar-la quan el porta-retalls del sistema conté text del KDE
Connect, i s'ha corregit la importació de fotos a o des d'ubicacions
remotes.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

L'Elisa és un reproductor musical senzill i popular, i aquest llançament
aporta la vista «Ara s'està reproduint» que el fa més atractiu.

Però no tot és sobre l'aparença. L'Elisa també és més funcional, ja que ara
s'hi pot accedir a través de la safata del sistema i minimitzar la llista de
reproducció a on es vulgui. Això vol dir que es pot mantenir l'audició
encara que es tanqui la finestra principal de l'Elisa. El mode de barreja
visual nou facilita reorganitzar la música a les llistes de reproducció, i
mostra la cançó següent que es reproduirà.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

El és l'aplicació avançada d'edició de vídeo del KDE, i la versió nova fa
més ràpida l'edició gràcies a la resolució configurable de la vista
prèvia. La vista prèvia també millora la visualització de la vista prèvia,
facilitant la selecció de les pistes quan s'estan editant totes al mateix
temps.

A sota de la línia de temps, l'equip ha millorat l'ajust de clips ajustant
només les pistes actives. També s'ha corregit la funcionalitat d'ajust de
grup i s'ha afegit una drecera per evitar ajustar en moure. Una altra cosa
que ara es pot fer és deixar anar directament els fitxers (vídeos, música i
imatges) des de l'explorador de fitxers a la línia de temps. La
funcionalitat nova «zoom als fotogrames clau» ajuda a fer un treball més
precís. Els professionals editors de vídeo i els usuaris que provinguin
d'altres eines d'edició apreciaran sense cap mena de dubte la funcionalitat
d'importació/exportació d'OpenTimelineIO.

Aquest llançament del Kdenlive també aporta moltes correccions d'errors, com
el del Seguidor de moviment, que estava trencat en alguns sistemes. Ara
permet afegir un filtre de compensació de to, així com afegir «mou i
redimensiona» a una forma de rotoscòpia. També s'ha solucionat la fallada de
l'assistent de DVD i l'error que afectava el canvi del volum
d'enregistrament des del mesclador. L'equip del Kdenlive també ha afegit les
funcionalitats d'etiquetatge, puntuació i filtratge a la safata del
projecte.

La versió nova del Kdenlive per al Windows també té correccions als diàlegs
de la línia de temps, que abans no acceptaven l'entrada del
teclat. Addicionalment, ara es pot seleccionar el dorsal d'àudio al Windows.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

El Yakuake és una finestra de terminal que es pot estirar des de la part superior de l'escriptori Plasma prement <kbd>F12</kbd>. és útil per executar instruccions ràpides a la línia d'ordres, i després sortir.

Quan s'obrin pestanyes noves o es divideixin subfinestres en aquesta versió
del Yakuake, ara es poden iniciar al mateix directori que la pestanya/plafó
actual. També es pot redimensionar verticalment la finestra del Yakuake
arrossegant-la per la seva barra inferior.

## Arranjament del sistema

La pàgina d'integració de comptes en línia de l'Arranjament del sistema
permet configurar qüestions com el vostre compte de Google, o definir
l'accés al vostre compte de Nextcloud. La pàgina s'ha redissenyat
completament, i ara té un aspecte net i modern, amb una funcionalitat molt
més fiable.

## [KDE Connect](https://kdeconnect.kde.org/)

El KDE Connect és una aplicació que ajuda a integrar el telèfon amb
l'escriptori i viceversa. Una de les funcionalitats més noves distribuïda
amb aquest llançament és la capacitat d'iniciar converses noves amb
l'aplicació de SMS.

Altres canvis inclouen la visualització de la reproducció actual dels
reproductors remots a la miniaplicació de reproducció de suports, un conjunt
nou d'icones d'estat de la connexió, i la gestió millorada de les
notificacions de trucada.

Al departament de correcció d'errors, s'ha resolt l'error que provocava que
la compartició de fitxers bloqués l'aplicació de trucades, i s'han
solucionat els problemes transferint fitxers realment grans.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

L'Spectacle, el programari de captura de pantalla del KDE, ha afegit
funcionalitats d'usabilitat i ha resolt diverses arestes. Una d'elles
impedia copia una captura de pantalla al porta-retalls quan s'havia obert
l'Spectacle des de la línia d'ordres amb l'opció «--nonotify». Encara que
això pot ser un problema molt particular, el fet és que ara es pot fer
exactament així. Igualment, les versions més antigues tenien problemes quan
s'intentava arrossegar una captura de pantalla des de l'Spectacle al patró
predeterminat de noms de fitxer que incloïen subcarpetes. Aquests problemes
s'han solucionat.

Una altra millora útil a la versió nova de l'Spectacle ve en la forma dels
botons «valors predeterminats» «Reverteix». Es poden usar per tornar a una
configuració prèvia, o inclús per eliminar qualsevol configuració
personalitzada anterior i restaurar-ho tot als seus valors predeterminats.

L'Spectacle no només recordarà la vostra configuració predeterminada, sinó
també la darrera àrea usada de captura de pantalla. Això ajuda a ser
coherent, fent més fàcil crear captures de la mateixa forma i mida que les
anteriors.

## [Krita](https://krita.org/en/) 4.2.9

S'han produït diverses actualitzacions tècniques de caire intern en aquest
llançament però també hi ha diverses funcionalitats noves com les dues
opcions noves que s'han afegit al pinzell d'Esborronat de color: «Aerògraf»
i «Rati de l'aerògraf». L'esborronat de color també ha guanyat el paràmetre
Rati que permet usar els diferents sensors per fer més plana la forma del
pinzell. La funcionalitat nova «Divisió de capa en màscara de selecció»
permet crear una capa nova per a cada color a la capa activa. I els traços
de pinzell més suaus que no parpellegen en passar sobre el llenç.

S'han produït diverses actualitzacions tècniques de caire intern en aquest
llançament però també els traços de pinzell més suaus que no parpellegen en
passar sobre el llenç. S'han afegit «Aerògraf» i «Rati de l'aerògraf» al
pinzell d'Esborronat de color, i un paràmetre Rati nou que permet usar els
diferents sensors per fer més plana la forma del pinzell. La funcionalitat
nova «Divisió de capa en màscara de selecció» permet crear una capa nova per
a cada color a la capa activa.

<iframe src=https://www.youtube.com/embed/fyc8-qgxAww width=560 height=315 frameborder=0 allowfullscreen=allowfullscreen></iframe>

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

El llançament 3.0.4 del Smb4K corregeix diversos problemes i fallades,
especialment al muntador. També fa que el Smb4K es pugui compilar amb la
versió 5.9 o inferiors de les Qt.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Aquesta versió troncal del connector que integra el Google Drive amb les
aplicacions del KDE afegeix la implementació per la funcionalitat «Shared
Drives» del Google Drive.

També inclou l'acció nova «Copia l'URL de Google al porta-retalls» al menú
contextual del Dolphin.
