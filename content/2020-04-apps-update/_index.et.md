---
pealkiri: KDE rakenduste uuendus aprillis 2020

publishDate: 2020-04-23 16:00:00

layout: page # don't translate

kokkuvõte: "Mis juhtus KDE rakendustega sel kuul"

type: announcement # don't translate
---

KDE rakenduste uus kimp on valmis! Nendes väljalasetes ootavad teid ees uued
omadused stabiilsuse täiustused  ja palju kasutajasõbralikke tööriistu, mis
aitavad teil tegutseda tõhusamalt ja produktiivsemalt.

Enamikus sinu lemmikrakendustes on kümneid vaimustavaid muutusi. Võtame
näiteks failihalduri Doplphin. Windowsi Samba jagatud ressursid on nüüd
täiel määral tuvastatavad.

Muusika mängimise valdkonnas on muusikamängija Elisa lisanud uusi
omadusi. Selles väljalaske pakutakse uut "Praegu mängitakse" vaadet, hõlpsat
ligipääsetavust süsteemisalve kaudu ja võimalust minimeerida esitusnimekiri
just siis, kui seda soovid. Tänu hiljuti lisatud visuaalse järjekorra
segamise režiimile on palju lihtsam muusikat esitusnimekirjades ümber
korraldada.

Meed on ainult üksikud näited kõige uue kohta, mida KDE rakendused sel kuul
pakuvad, Loe edasi ja tutvu kõigega, mida me sull seekord pakume.

## [Okular](https://okular.kde.org)

Okular on KDE rakendus, mis lubab lugeda PDF-faile, Markdown-dokumente,
koomikseid ja veel paljusid dokumendivorminguid. Selles väljalaskes pakub
Okular mõningaid hõlbustustäiustusi nii töölaua kui ka puuteekraani
kasutajatele. Esimestele on mõeldud alati sujuv kerimine: nii siis, kui
kasutad hiiretast, kui ka siis, kui kasutad klaviatuuri. Puuteekraani
kasutajatele pakub Okulari uusim versioon inertskerimist.

{{<video src="okular.mp4" caption="Okulari sujuv kerimine. Illustratsioon: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

KDE võimas failihaldur Dolphin lisas uusi omadusi, mis lubavad senisest
paremini kasutada võrgufailisüsteeme, näiteks Samba jagatud ressursse ja SSH
servereid. Võrguasukohtade filme saab nüüd vaadata ilma neid alla laadimata,
vaid suunates andmevoo Dolphini vahendusel pilvest oma videomängijasse.

Samamoodi on nüüd ühendumine Windowsi Samba jagatud ressurssidega ja kõigi
nende pakutavate tenuste kasutamine senisest tunduvalt hõlpsam, sest Samba
jagatud ressursse tuvastatakse WS Discovery protokolli abil, mida
kasutatakse Windowsi uuemates versioonides. See võimaldab oma Linuxi masina
sujuvalt ja raskusteta lõimida Windowsi võrku.

Kasutamislihtsuse valdkonnas ei lähe terminalipaneli fookusse toomiseks ja fookusest viimiseks enam tingimata vaja hiirt. Nüüd võib selleks kasutada lihtsalt kiirklahvi <kbd>Ctrl</kbd> + <kbd>tõstuklahv</kbd> + <kbd>F4</kbd>. Veel üks kaua igatsetud omadus on failide otsimine mitte ainult nime või sisu, vaid ka sildi järgi.

7Zip-failide toetus on nüüd Dolphinisse sisse ehitatud, mis tähendab, et
neis tihendatud failides saab liikuda sama hõlpsalt nagu failisüsteemi
kataloogides. Kui paigaldada väline otsimisrakendus, näiteks KFind, lubab
Dolphin linki luues neile kiiresti ligi pääseda.

Viimaks on üks näiliselt tagasihoidlik omadus, mis aga võib palju aega säästa, uhiuus kloonimisvõimalus. Vali üks või rohkem faili või kataloogi, vajuta <kbd>Ctrl</kbd> + <kbd>D</kbd> (või tee paremklõps ja vali "Klooni siin") ja ongi valmis! Kõigi valitud elementide koopia ilmub originaali kõrvale.

![Dolphini kloonimisvõimalus](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

Lokalize on tööriist, mis laseb tõlkida gettext- ja xliff-faile. Nüüd toetab
see ka LanguageTool'i grammatikakontrolli, mis tõlkija abiks tõstab tekstis
esile grammatikavead.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

KMail on KDE kõige menukam e-posti klient ja kuulub rakendustekomplekti
Kontact, mis võimaldab hallata kirju, kontakte ja ülesandeid.

Selles väljalaskes on mitmeid täiustusi. Kõigepealt saab e-kirju nüüd
hõlpsasti eksportida PDF-vormingusse ning Markdown-vormingus kirju kuvatakse
paremini. Omajagu tähelepanu on pööratud turbeküsimustele. Nii näitab KMail
nüüd hoiatust, kui kirjakoostaja avaneb linki klõpsates, mis palub lisada
kirjale faili.

KMail on KDE kõige menukam e-posti klient ja kuulub rakendustekomplekti
Kontact, mis võimaldab hallata kirju, kontakte ja ülesandeid. Selles
väljalaskes on mitmeid täiustusi. Kõigepealt saab e-kirju nüüd hõlpsasti
eksportida PDF-vormingusse ning Markdown-vormingus kirju kuvatakse
paremini. Omajagu tähelepanu on pööratud turbeküsimustele. Nii näitab KMail
nüüd hoiatust, kui kirjakoostaja avaneb linki klõpsates, mis palub lisada
kirjale faili

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

Konsool on terminaliemulaator, mis lubab käsureal käivitada käske, protsesse ja rakendusi. Konsooli uusimas versioonis saab kiirklahviga <kbd>Alt</kbd> + arvklahv hüpata otse esimesele üheksale avatud kaardile.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

Gwenview on üls KDE menukamaid pildinäitajaid ning selles versioon on
lahenduse saanud kaks suurt probleemi. Rakendus enam ei hangu käivitamisel,
kui süsteemi lõikepuhver sisaldab KDE Connect'i teksti, samuti on parandatud
fotode import võrguasukohtadest või eksport võrguasukohtadesse.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

Elisa on lihtne ja populaarne muusikamängija, mis selles väljalaskes pakub
suurema atraktiivsuse nimel omadust "Praegu mängitakse".

Ent asi ei piirdu pelgalt välimusega. Elisa on ka funktsionaalsem,
võimaldades nüüd ennast kasutada süsteemisalve vahendusel ning minimeerida
esitusnimekirja, millal iganes sa seda soovid. See tähendab, et oma
lemmikmuusikat saab kuulata ka siis, kui Elisa peaaken on suletud. Uus
visuaalse segamise režiim luba vähesema vaevaga muusikat esitusnimekirjades
ümber korraldada ja ühtlasi näha, milline pala esitatakse järgmisena.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

Kdenlive on KDE võimas videoredaktor, mille uus versioon võimaldab senisest
kiiremat video muutmist änu eelvaatluse seadistatavale
lahutusvõimele.Eelvaatlus parandab ka mitme raja korraga nägemise võimalust,
lubades hõlpsasti valida radu kõigi nende seast, mida sa parajsti korraga
redigeerid.

Ajateljel on täiustatud klippide tõmmet, et neid tõmmataks ainult
aktiivsetele radadele. Samuti on parandatud rühmatõmbe võimalust ja lisatud
kiirklahv tõmbamise vältimiseks liigutamisel. Veel üks asi, mida nüüd saab
teha, on failide (video, muusika, piltide) lohistamine otse failihaldurist
ajateljele. Uus omadus "suurendus võtmekaadritel" aitab kaasa video
täpsemale redigeerimisele. Nii professionaalsed videoredaktorid kui ka
tavalised kasutajad kindlasti hindavad ka OpenTimelineIO impordi/ekspordi
võimalust.

Käesolev Kdenlive'i väljalase toob kaasa ka hukga veaparandusi, näiteks
liikumise jälgijas, mis oli mitmes süsteemis päris katki. Nüüd lubab see
lisada helikõrguse kompenseerimise filtri, samuti "liigutamise ja suuruse
muutmise" võimaluse rotoskoopiakujundile. Tõsiselt võeti ette DVD nõustaja
krahh ja viga, on oli pikka aega salvestamise helitugevuse muutmist
mikseris. Samuti on Kdenlive'i meeskond lisanud projektipuusse sildistamise,
hinnangu andmise ja filtreerimise võimaluse.

Kdenlive'i uus Windowsi versioon pakub samuti ajatelje dialoogide parandusi,
mis varem ei tunnistanud klaviatuurisisendit. Lisaks saab Windowsis nüüd
valida heli taustaprogrammi.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

Yakuake on terminaliaken, mida saab Plasma töölaual ülalt alla lahti kerida klahvile <kbd>F12</kbd> vajutades. See on käepärane tööriist kiirete juhiste andmiseks käsureal ja siis mujale liikumiseks.

Yakuake käesolevas versioonis uusi kaarte avades või paneele tükeldades saab
nüüd nendega alustada samas kataloogis nagu aktiivses
kaardis/paneelis. Samuti saab Yakuake akende suurust püstsuunas muuta
alumist riba lohistades.

## Süsteemi seadistused

Süsteemi seadistuste võrgukontode lõimimise lehekülg laseb seadistada
näiteks oma Google'i kontot või panna paika ligipääs Nexycloudi kontole. See
lehekülg on saanud põhjaliku ümberkorralduse, olles nüüd puhtama ja moodsama
välimusega ning pakkudes usaldusväärsemat funktsionaalsust.

## [KDE Connect](https://kdeconnect.kde.org/)

KDE Connect on rakendus, mis lubab lõimida telefoni töölauaga ja
vastupidi. Üks uuemaid võimalusi, mida käesolev väljalase kaasa toob,
võimaldab alustada SMS-rakendusega uusi vestlusi.

Muudest muutustest võib ära märkida võrgumeedianängijate näitamise
meediamängija apletis, uus ühenduse olekuikoonide komplekti ning märguannete
käitlemise täiustamise.

Vigade parandamise valdkonnas parandati viga, mis põhjustas failide
jagamisel rakenduse väljakutsumise blokkimise, samuti lahendati probleemid
väga-väga suurte failide ülekandmisel.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

KDE ekraanipiltide tegemise rakendus Spectacle sai juurde mõned kasutamist
hõlbustavad omadused ning lahendada suudeti mõned piinlikud vead. Üks neist
takistas kopeerida ekraanipilti lõikepuhvrisse, kui Spectacle oli avatud
käsurealt. See ei pruugi olla suur häda, aga igatahes on nüüd see
võimalik. Samuti oli vanemates versioonides probleeme ekraanipildi
lohistamisel Spectacle'ist vaikimisi failinimemustrisse, mis sisaldas
alamkatalooge. Need probleemid on nüüd lahendatud.

Veel üks Spectacle'i uue versiooni tulus täiustus on nuppude
"Vaikeväärtused" ja "Taasta" lisamine. Nendega saab tagasi pöörduda varasema
seadistuse juurde või isegi eemaldada kõik varasemad kohandatud seadistused
ja taastada kõik vaikeväärtustele.

Spectacle mitte ainult ei jäta meelde vaikimisi seadistust, vaid ka viimati
kasutatud ekraanipildi tegemise piirkonna. See võimaldab tagada ühtlust,
lubades vähema vaevaga teha sama kuju ja suurusega ekraanipilte nagu
eelnevalt.

## [Krita](https://krita.org/en/) 4.2.9

Selles väljalaskes on hulk varju jäävaid tehnilisi uuendusi, aga ka mõned
uued omadused, näiteks kaks uut valikut värvimäärimispintslis: pihusti ja
pihustitase- Värvimäärimisele lisati ka uus proportsiooni seadistus, mis
lubab eri sensorite alusel muuta pintsli kuju lamedamaks. Uus omadus
"Tükelda kiht valikumaski" lubab luua uue kihi aktiivse kihi kõigi värvide
alusel. Mahedamad joonistuspintsli kontuurid ei plingi enam, kui hiirekursor
lõuendi kohale viia.

Selles väljalaskes on hulk varju jäävaid tehnilisi uuendusi, aga ka mõned
uued omadused, näiteks kaks uut valikut värvimäärimispintslis: pihusti ja
pihustitase- Värvimäärimisele lisati ka uus proportsiooni seadistus, mis
lubab eri sensorite alusel muuta pintsli kuju lamedamaks. Uus omadus
"Tükelda kiht valikumaski" lubab luua uue kihi aktiivse kihi kõigi värvide
alusel. Mahedamad joonistuspintsli kontuurid ei plingi enam, kui hiirekursor
lõuendi kohale viia

<iframe src=https://www.youtube.com/embed/fyc8-qgxAww width=560 height=315 frameborder=0 allowfullscreen=allowfullscreen></iframe>

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

Smb4K väljalase 3.04 parandab ära mitmed vead ja krahhid, eriti
ühendajas. Samuti on Smb4K kompileeritav Qt versiooni 5.9 ja vanemate peal.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Google Drive'i KDE rakendustega lõimiva plugina suur väljalase lisab Google
Drive'i jagatud ketaste toetuse.

Samuti lisab see Dolphini kontekstimenüüsse uue kirje "Kopeeri Google'i URL
lõikepuhvrisse".
