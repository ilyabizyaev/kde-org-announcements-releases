---
title: Actualització de les aplicacions del KDE de gener de 2020

publishDate: 2020-01-09 12:00:00

layout: page # don't translate

summary: "Què ha passat a les aplicacions del KDE aquest mes"

type: announcement # don't translate
---

Aquest és l'any 2020, i estem vivint en el futur. Vegem que ens han portat
les aplicacions del KDE en el darrer mes!

## El KTimeTracker s'ha adaptat als Frameworks 5 del KDE

La llargament esperada versió modernitzada del KTimeTracker finalment s'ha
publicat. L'aplicació és un seguidor personal de temps per persones
atrafegades que ara és disponible per al Linux, FreeBSD i Windows. Durant el
2019 s'ha adaptat a les Qt5 i als Frameworks del KDE després d'estar sense
manteniment des del voltant de 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

La nova versió també s'ha polit i modernitzat lleugerament. Les
funcionalitats noves més destacades són el diàleg nou d'edició del temps de
les tasques i la vista prèvia en directe del diàleg d'exportació com es veu
a la imatge de sota.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Diàleg d'exportació del KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/ És disponible a
través de la vostra distribució de Linux o com un [instal·lador pel
Windows](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) i inclús
hi ha [compilacions no provades pel
MacOS](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) que
es construeixen nocturnament.

[La publicació s'ha anunciat al blog del mantenidor Alexander
Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

El programa d'astronomia [KStars ha incorporat funcionalitats noves a la
3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Les imatges poden tenir canvis subtils a les ombres, tons mitjos i llums
intenses que permeten veure les estrelles més dèbils.

És fascinant [llegir
sobre](https://sternenkarten.com/2019/12/27/kstars-clines/) les
constel·lacions alternatives de la cultura astrològica occidental.

{{< img src="Screenshot_20200102_162452.jpg" caption="Constel·lacions alternatives" alt="Cultura astrològica occidental" >}}

El [KStars és disponible](https://edu.kde.org/kstars/) per a l'Android,
Windows, MacOS, la botiga Snap i a la vostra distribució Linux.

## Frameworks comuns - KNewStuff

Aquí, a les actualitzacions d'aplicacions posem el focus a les aplicacions
per sobre de les biblioteques de codificació. Però les funcionalitats noves
a les biblioteques comunes signifiquen funcionalitats noves a totes les
aplicacions :)

Aquest mes s'ha produït el redisseny de la IU del KNewStuff, el Framework
per baixar complements per a les aplicacions. S'han redissenyat els diàlegs
de navegació i baixada i ara es pot filtrar la secció de
comentaris. Apareixerà a totes les aplicacions i mòduls de configuració
després d'iniciar el mòdul del Tema global de l'Arranjament del sistema.

{{< img src="knewstuff-comments.png" caption="Filtres als comentaris" alt="Filtres als comentaris" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Diàleg de navegació redissenyat" alt="Diàleg de navegació redissenyat" style="width: 800px">}}

## Correcció d'errors

L'[actualització de correcció d'errors mensuals del KDevelop
5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) corregeix un
problema de fa temps en que les capçaleres GPL i LGPL es
barrejaven. Obteniu-lo des de la vostra distribució o des d'Appimage per
assegurar que les llicències són correctes.

El [Latte Dock
0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html)
corregeix diverses funcionalitats amb les Qt 5.14 i elimina vàries fallades.

Els connectors del Dolphin 19.12.1 corregeixen un diàleg trencat del Commit
de SVN.

S'ha millorat la indexació de fitxers a l'Elisa. També s'han corregit
diversos problemes de compilació a l'Android i sense el Baloo.

La publicació nova del KPat s'ha declarat que no té restriccions
[OARS](https://hughsie.github.io/oars/index.html) d'edat rellevants.

L'Okular ha corregit una fallada en tancar el diàleg de vista prèvia de la
impressió.

La publicació d'aquest mes de l'editor de vídeo Kdenlive té un nombre
impressionant de correccions. La millor de totes és l'actualització de les
[captures de
pantalla](https://kde.org/applications/multimedia/org.kde.kdenlive) usades a
la metainformació. També té dotzenes de millores i correccions a la gestió
de la línia de temps i a la vista prèvia.

Ara hi ha un suport nou del JavaScript al client
[LSP](https://langserver.org/) del Kate.

## Gaudiu del KDE a la botiga del Flathub

El KDE està incorporant totes les botigues d'aplicacions. Ara podem lliurar
més i més dels nostres programes directament a l'usuari. Una de les botigues
d'aplicacions principals del Linux és [Flathub](https://flathub.org/home)
que usa el format FlatPak.

Potser ja teniu Flatpak i Flathub configurats al vostre sistema i estan
preparats per usar-se al Discover o altres instal·ladors d'aplicacions. Per
exemple, el KDE Neon els ha establert de manera predeterminada a les
instal·lacions des de fa un any. Si no, és un [procés de
configuració](https://flatpak.org/setup/) ràpid per a totes les
distribucions principals.

Si teniu interès en els detalls tècnics podeu explorar el [repositori
d'empaquetament Flatpak del
KDE](https://phabricator.kde.org/source/flatpak-kde-applications/) i llegir
la [guia Flatpak del
KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Però probablement hi haurà més interès en donar una ullada a les aplicacions
que hi ha del [KDE a la botiga
Flathub](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE a la Flathub" style="width: 800px" >}}

## Ara el LabPlot és a Chocolatey

El [Chocolatey](https://chocolatey.org/) és un gestor de paquets per al
Windows. Si voleu un control complet sobre el programari que està instal·lat
a la vostra màquina Windows o a les màquines de tota l'oficina, llavors el
Chocolatey us dóna un control fàcil com el que esteu acostumat amb el Linux.

El [LabPlot](https://chocolatey.org/packages/labplot) és l'aplicació del KDE
per fer gràfics i anàlisis interactius de dades científiques i ara és
disponibles a través de Chocolatey. Proveu-lo!

El [blog del
LabPlot](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="El LabPlot al Chocolatey" style="width: 800px" >}}

## Actualitzacions del lloc web

El recentment reviscolat equip web del KDE ha estat actualitzant una pila
dels nostres llocs temàtics. El lloc web llançat de nou del
[KPhotoAlbum](https://www.kphotoalbum.org/) és un bon exemple, actualitzat i
refrescat per a l'aplicació d'emmagatzematge i cerca de fotos.

{{< img src="kphotoalbum.png" alt="Lloc web nou del KPhotoAlbum" >}}

I si voleu descobrir un reproductor local senzill de música però amb
funcionalitats completes però estàveu avergonyit de l'aparença de l'antic
lloc web, el [JuK](https://juk.kde.org/) també té un lloc web actualitzat.

## Llançaments 19.12.1

Varis dels nostres projectes publiquen en la seva pròpia escala de temps i
altres es publiquen conjuntament. El paquet de projectes 19.12.1 s'ha
publicat avui i aviat hauria de ser disponible a través de les botigues
d'aplicacions i de les distribucions. Vegeu la [pàgina del llançament
19.12.1](https://www.kde.org/info/releases-19.12.1.php). Aquest paquet
s'havia anomenat prèviament Aplicacions del KDE però s'ha descatalogat per
esdevenir un servei de llançaments per evitar confusions amb totes les
altres aplicacions del KDE i perquè són una dotzena de productes diferents
en lloc d'un únic grup.
