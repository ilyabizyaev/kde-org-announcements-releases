---
pealkiri: KDE rakenduste uuendus jaanuaris 2020

publishDate: 2020-01-09 12:00:00

layout: page # don't translate

kokkuvõte: "Mis juhtus KDE rakendustega sel kuul"

type: announcement # don't translate
---

Aasta on nüüd 2020, me elame tulevikus, niisiis, vaatame, mida KDE
rakendused meile sel kuul pakuvad!

## KTimeTracker porditi KDE Frameworks 5 peale

Ammu oodatud KTimeTrackeri uuendatud versioon on viimaks ilmavalgust
näinud. See rakendus kujundab endast personaalset ajakasutuse jälgijat väga
hõivatud inimestele ning nüüd saadaval Linuxis, FreeBSD-s ja
Windowsis. Pärast seda, kui rakendus 2013. aasta paiku unarule jäi, porditi
see nüüd 2019. aastal Qt5 ja KDE Frameworksi peale.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

Uut versiooni on mõnevõrra lihvitud ja kaasajastatud. Kõige silmatorkavamad
uuendused on ülesande aja muutmise dialoog ja ekspordidialoogi reaalajas
eelvaatlus, mida näeb ka pildil.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="KTimeTrackeri ekspordidialoog" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/ on saadaval sinu
Linuxi distributsioonis või [Windowsi
paigaldajas](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) ning
saada on isegi [testimata
MacOSi](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/)
"ööversioon".

[Väljalaskest andis oma ajaveebis teada rakenduse hooldaja Alexander
Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

Astronoomiarakendus [KStars sai versioonis 3.3.9 uusi
omadusi](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Piltidel saab muuta heledaid, tumedaid ja keskmisi toone, mis lubab näha ka
kõige tuhmimaid tähti.

Lääne taevakultuuri vaimustavalt välja paistvad alternatiivsed tähtkujud
([loe lähemalt](https://sternenkarten.com/2019/12/27/kstars-clines/)).

{{< img src="Screenshot_20200102_162452.jpg" caption="Alternative constellations" alt="Lääne taevakultuur" >}}

[KStars on saadaval](https://edu.kde.org/kstars/) Androidile, Windowsile,
MacOS-ile, Snapi hoidlas ja sinu Linuxi distributsiooonis.

## Frameworksi-ülene KNewStuff

Rakenduste uuendustest kõneldes keskendume tavaliselt rakendustele, mitte
nii väga neid toetavatele teekidele. Kuid uued võimalused ühistes teekides
tähendavad ka uusi võimalusi kõigis rakendustes!

Sel kuul kujundati ümber KNewStuffi kasutajaliides, mis teatavasti on
raamistik rakenduste lisandite allalaadimiseks. Sirvimise ja allalaadimise
dialoogi kujundust muudeti ning kommentaaride sektsiooni saab nüüd
filtreerida. Seda peaks näha olema peagi pärast seda, kui Süsteemi
seadistustes on käivitatud globaalse teema moodul.

{{< img src="knewstuff-comments.png" caption="Filters on Comments" alt="Filtreerimine kommentaaride järgi" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Uue kujundusega sirvimisdialoog" alt="Uue kujundusega sirvimisdialoog" style="width: 800px">}}

## Veaparandused

[KDevelopi igakuise veaparanduste uuenduse versiooniga
5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) parandati ära
pikemat aega muret valmistanud probleem, mis segas omavahel GPL ja LGPL
päiseid. Paigalda see oma distributsioonis või Appimagest, et litsentsidega
oleks jätkuvalt kõik korras.

[Latte Dock
0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html)
parandas mõne omaduse probleemid Qt 5.14-ga ja kõrvaldas mõned krahhi
põhjustavad asjad.

Dolphini pluginate väljalaskes 19.12.1 parandati ära SVN-i sissekandmise
dialoog.

Elisas täiustati failide indekseerimist. Samuti parandati mõned
kompileerimisprobleemid Androidis ja ilma Baloota.

KPati uus väljalase on nüüd
[OARS](https://hughsie.github.io/oars/index.html)-i vanusepiiranguteta.

Okularis on parandatud viga, mis tõi trükkimise eelvaatluse dialoogi
sulgemisel kaasa krahhi.

Videoredaktori Kdenlive sellekuises väljalaskes on muljet avaldav hulk
veaparandusi, millest parim on metainfos kasutatavate
[ekraanipiltide](https://kde.org/applications/multimedia/org.kde.kdenlive)
uuendamine. Samuti pakub see väljalase kümneid täiustusi ja parandusi
ajateljel ja eelvaatluse käitlemisel.

Kate [LSP](https://langserver.org/) klient toetab nüüd JavaScripti.

## KDE viimaks Flathubi poes

KDE püüab kohal olla kõigis rakendustepoodides. Nii saame üha rohkem
rakendusi pakkuda otse kasutajale. Üks Linuxi juhtivaid rakendustepoode on
[Flathub](https://flathub.org/home), kus kasutatakse FlatPaki vormingut.

Pole võimatu, et sinu süsteemis on Flatpak ja Flathub juba seadistatud ning
valmis kasutamiseks Discoveris või mõnes muus rakenduste
paigaldajas. Näiteks KDE Neon kasutab seda tarkvara paigaldamiseks nüüd juba
üle aasta. Kui sinu distributsioon seda veel ei võimalda, siis on kõigi
suuremate distributsioonide puhul võimalik seda kiiresti [paika
panna](https://flatpak.org/setup/).

Kui tunned huvi tehniliste üksikasjade vastu, võid sirvida [KDE Flatpaki
paketihoidlat](https://phabricator.kde.org/source/flatpak-kde-applications/)
ja lugeda [KDE Flatpaki
juhiseid](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Aga tõenäoliselt oled huvitatud lihtsalt rakendustest, millisel juhul vaata,
mida pakub [Flathubi pood KDE puhul](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE Flathubis" style="width: 800px" >}}

## LabPlot on nüüd Chocolateys

[Chocolatey](https://chocolatey.org/) on Windowsi tarkvarahaldur. Kui soovid
täielikku kontrolli selle üle, milline tarkvara on paigaldatud sinu Windowsi
masinasse või tervesse masinaparki, siis annab just Chocolatey sulle selle
üle samasuguse hõlpsa kontrollivõimaluse, millega oled harjunud Linuxis.

[LabPlot](https://chocolatey.org/packages/labplot) on KDE teaduslike andmete
interaktiivsete joonistena esitamise ja analüüsimise rakendus, mis on nüüd
saadaval ka Chocolateys. Proovi seda!

[LabPloti
ajaveeb](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot Chocolateys" style="width: 800px" >}}

## Veebilehekülje uuendused

Hiljaaegu taassünni läbi elanud KDE veebimeeskond on asunud uuendama tervet
hulka meie vanamoeliste teemadega lehekülgi. Üks armsamaid näiteid on
vastselt uuenduskuuri läbi teinud fotode salvestamise ja otsimise rakenduse
[KPhotoAlbum](https://www.kphotoalbum.org/)i veebilehekülg.

{{< img src="kphotoalbum.png" alt="KPhotoAlbumi uus veebilehekülg" >}}

Kui soovid aga näidata teistelegi hõlpsasti kasutatavat, aga siiski kõiki
võimalusi pakkuvat muusikamängijat, ent oled seni häbenenud vanamoelise
välimusega veebilehte, siis sai [JuK](https://juk.kde.org/) just äsja samuti
tugevasti uuendatud veebilehe kujunduse.

## Väljalasked 19.12.1

Mõned meie rakendused lasevad uusi versioone välja oma ajakava järgi, mõned
ilmuvad paljukesi korraga. Täna nägi ilmavalgust projektikimp 19.12.1, mis
varsti peaks jõudma ka rakenduste hoidlatesse ja distributsioonidesse. Vt
[19.12.1
väljalaskelehekülge](https://www.kde.org/info/releases-19.12.1.php). Varem
nimetati seda kimpu üldnimetusega KDE rakendused, aga see muudeti
väljalasketeenuseks, et vältida segaduse tekkimist kõigi teiste KDE
rakendustega, aga ka seepärast, et see pole tegelikult üks tervik, vaid
koosneb kümnetest eri rakendustest.
