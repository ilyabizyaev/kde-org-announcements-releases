---
title: 2020년 1월 KDE 앱 업데이트

publishDate: 2020-01-09 12:00:00

layout: page # don't translate

summary: "이번 달에 KDE 프로그램에서는 어떤 일이 있었나요"

type: announcement # don't translate
---

올해는 2020년입니다. 미래가 다가왔죠. 지난 한 달 동안의 KDE 앱 변경 사항을 알아 봅시다!

## KTimeTracker가 KDE 프레임워크 5로 이식됨

KTimeTracker의 새로운 버전이 드디어 출시되었습니다. 바쁜 사람들의 개인 시간 추적기 프로그램은 리눅스, FreeBSD,
Windows에서 사용할 수 있습니다. 2013년 주변으로 관리되지 않다가 2019년 한 해 동안 Qt5와 KDE 프레임워크로
이식되었습니다.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

사용자 인터페이스를 다듬음과 동시에 새로운 주요 기능으로 내보내기 대화 상자에서 라이브 미리 보기 기능을 지원합니다(아래 그림 참조).

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="KTimeTracker의 내보내기 대화 상자" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/ 리눅스 배포판이나 [Windows
설치 프로그램](https://download.kde.org/stable/ktimetracker/5.0.1/win64/)을 사용할 수
있으며, [테스트되지 않은 MacOS 일간
빌드](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/)도 있습니다..

[관리자 Alexander Potashev의 블로그에 자세한 릴리스 공지가
있습니다](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

천문학 프로그램 [KStars 3.3.9에 새로운 기능이
추가되었습니다](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

그림자, 미드톤, 강조 그림에 미세한 변경 사항을 적용할 수 있습니다. 어두운 별을 더 쉽게 볼 수 있습니다.

Western Sky Culture의 대체 은하 기능도 [읽어
보세요](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Alternative constellations" alt="Western Sky Culture" >}}

[KStars](https://edu.kde.org/kstars/)는 안드로이드, Windows, macOS, Shap 스토어 및 리눅스
배포판에서 만나 볼 수 있습니다.

## 공통 프레임워크 - KNewStuff

이 앱 업데이트에서는 코딩 라이브러리보다는 앱에 초점을 맞춥니다. 그러나 공통 라이브러리에 있는 새로운 기능 또한 앱에 반영됩니다 :)

이번 달에는 프로그램의 추가 기능을 다운로드하는 KNewStuff의 UI 업데이트가 있었습니다. 탐색과 다운로드 대화 상자를 개선했으며
댓글을 필터링할 수 있습니다. 시스템 설정의 전역 테마 모듈을 시작으로 모든 앱과 설정 모듈에 변경 사항이 나타납니다.

{{< img src="knewstuff-comments.png" caption="댓글 필터" alt="댓글 필터" style="width: 800px">}}

{{< img src="knewstuff.png" caption="탐색 창 재설계" alt="탐색 창 재설계" style="width: 800px">}}

## 버그 수정

[KDevelop 월간 버그 수정 업데이트
5.4.6](https://www.kdevelop.org/news/kdevelop-546-released)에서는 GPL과 LGPL 헤더가
뒤바뀌는 문제를 해결했습니다. 배포판이나 Appimage 업데이트를 설치해서 라이선스 조항이 올바르게 적용되어 있는지 확인하세요.

[Latte 독
0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html)은
Qt 5.14 사용 시 문제 및 여러 충돌 사항을 개선했습니다.

Dolphin 플러그인 19.12.1: SVN 커밋 대화 상자 문제를 해결했습니다.

Elisa의 파일 색인 기능이 개선되었습니다. 안드로이드 및 Baloo가 없는 환경에서의 컴파일 문제를 해결했습니다.

KPat의 새 릴리스는 [OARS](https://hughsie.github.io/oars/index.html) 기준 연령 제한 없음으로
표기되었습니다.

Okular에서 인쇄 미리 보기 대화 상자를 닫을 때 충돌 문제를 수정했습니다.

이번 달의 Kdenlive 동영상 편집기 릴리스에는 버그 수정 및 메타 정보에 사용되는
[스크린샷](https://kde.org/applications/multimedia/org.kde.kdenlive) 업데이트가
있었습니다. 타임라인 기능 및 미리 보기 처리의 다양한 기능 개선과 버그 수정이 있었습니다.

Kate의 [LSP](https://langserver.org/) 클라이언트에 자바스크립트 지원이 추가되었습니다.

## Flathub 스토어에서 KDE를 만나 보세요

보다 많은 사용자들이 프로그램을 직접 써 볼 수 있도록 KDE는 여러 앱 스토어로 뻗어 나갑니다. 리눅스 세계의 주요 앱 스토어 중에는
FlatPak 형식을 사용하는 [Flathub](https://flathub.org/home)가 있습니다.

둘러보기 및 다른 앱 설치 도구에서 Flatpak과 Flathub를 사용하도록 이미 설정되어 있을 수도 있습니다. KDE neon에는
이미 이 설정이 기본값으로 포함되어 있습니다. 대부분 리눅스 배포판에서 쉽게 [설치](https://flatpak.org/setup/)할
수 있습니다.

기술적인 정보를 더 알아 보려면 [KDE Flatpak 패키지
저장소](https://phabricator.kde.org/source/flatpak-kde-applications/)와 [KDE
Flatpak 가이드](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak)를 읽어
보십시오.

그러나 대부분 사용자라면 앱 설치에 관심이 있을 것이므로 [Flathub 스토어의
KDE](https://flathub.org/apps/search/kde) 앱을 찾아 보세요.

{{< img src="flathub.png" alt="Flathub의 KDE" style="width: 800px" >}}

## Chocolatey에서 LabPlot 사용 가능

[Chocolatey](https://chocolatey.org/)는 Windows용 패키지 관리자입니다. 내 Windows 시스템이나
전체 사무실에 설치된 소프트웨어를 통제하고 싶다면 Chocolatey는 리눅스 시스템을 관리하는 것처럼 도와 줍니다.

[LabPlot](https://chocolatey.org/packages/labplot)은 인터랙티브 그래핑, 과학 데이터 분석을
지원하는 KDE 앱이며 이제 Chocolatey에서 만나 볼 수 있습니다. 체험해 보세요!

[LabPlot
블로그](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="Chocolatey의 LabPlot" style="width: 800px" >}}

## 웹 사이트 업데이트

최근 개편된 KDE 웹 팀에서는 오래된 웹 사이트를 업데이트하기 시작했습니다. 사진 저장 및 검색 앱인
[KPhotoAlbum](https://www.kphotoalbum.org/)의 웹 사이트는 웹 팀의 최근 결과물을 보여 줍니다.

{{< img src="kphotoalbum.png" alt="새로운KPhotoAlbum 웹 사이트" >}}

비록 기능이 강력한 음악 재생기지만 오래되어 보였던 웹 사이트에서 소개하고 있었던 [JuK](https://juk.kde.org/)의 웹
사이트도 업데이트되었습니다.

## 릴리스 19.12.1

일부 프로젝트는 개별적인 일정에 따라 릴리스되며 일부는 다 같이 릴리스됩니다. 19.12.1 프로젝트 번들은 오늘 출시되었으며, 곧 앱
스토어와 배포판에서 사용할 수 있습니다. 자세한 정보는 [19.12.1 릴리스
페이지](https://www.kde.org/info/releases-19.12.1.php)를 참조하십시오. 이 번들은 KDE
프로그램으로 불렸으나, KDE의 다른 프로그램과의 혼동 방지 및 개별 프로젝트를 더 잘 드러내기 위해서 더 이상 이 이름을 사용하지
않습니다.
