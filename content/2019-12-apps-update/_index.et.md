---
pealkiri: KDE rakenduste uuendus detsembris 2019

kokkuvõte: "Mis juhtus KDE rakendustega sel kuul"

publishDate: 2019-12-12 13:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

# KDE rakenduste uued versioonid detsembris

KDE rakenduste uute versioonide väljalaskmine väljendab KDE kestvat soovi ja
pingutust pakkuda kasutajatele võimalikult täielikku ja uhiuut rohkete
võimalustega, kauni välimusega ja kasulike rakenduste komplekti.

Nüüd on saadaval uued versioonid KDE failisirvijast Dolphin, ühest
täiuslikumast avatud lähtekoodiga videoredaktorist Kdenlive,
dokumendinäitajast Okular, KDE pildinäitajast Gwenview ja veel paljudest
sinu lemmikutest KDE rakendustest ja tööriistadest. Kõiki neid on parandatud
ja täiustatud, muudetud kiiremaks ja stabiilsemaks ning neile on lisatud
uusi vaimustavaid omadusi. KDE rakenduste uued versioonid lubavad teil olla
loovad ja tootlikud ning samal ajal nautida KDE rakenduste kasutamise
lihtsust.

Me loodame, et te naudite kõigi KDE rakenduste uudseid omadusi ja täiustusi!

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) ärkas uuesti ellu

[Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan)
võttis detsembris ette uue suure väljalaske. KDE projektide kavandamise ja
haldamise tööriistale on see suur samm edasi, sest eelmine versioon jääb
juba peaaegu kahe aasta taha.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

Plan abistab nii suurte kui ka väikeste paljude ressurssidega projektide
haldamisel. Projekti koostamiseks pakub Plan välja erinevat tüüpi ülesannete
sõltuvusi ja ajalisi piiranguid. Võimalik on panna paika oma ülesanded,
hinnata iga ülesande täitmiseks vajalikku pingutust, omistada nende
täitmiseks ressursid ja siis määrata kindlaks projekti ajakava vastavalt
vajadusele ja ressursside saadavusele.

Üks Plani tugevaid külgi on suurepärane Gantti diagrammide toetus. Gantti
diagrammid koosnevad rõhttulpadest, mis kujutavad visuaalselt ajakava ning
on abiks projekti konkreetsete ülesannete plaanimisel, koordineerimisel ja
jälgimisel. Gantti diagrammide kasutamine Planis aitab paremini jälgida oma
projekti töövoogu.

## Keerame [Kdenlive](https://kdenlive.org)'is heli põhja

[Kdenlive](https://kdenlive.org)'i arendajad on usinalt lisanud uusi
võimalusi ja parandanud avastatud vigu. Ainuüksi selle versiooni tarbeks
tehti üle 200 sissekande.

Rohket vaeva nähti heli toetamise parandamisel. Vigade lahendamise poole
pealt saadi lahti tohutust mälu tarbimise probleemist, muudeti
helipisipildid taas toimivaks ja kiirendati tunduvalt nende salvestamist.

Veel vaimustavam on see, et Kdenlive pakub nüüd imetabast uut helimikserit
(vt pilti). Samuti on arendajad lisanud klipimonitori ja projektipuusse uue
heliklipi kuvamise võimaluse, mis võimaldab paremini sünkroonida liikuvaid
pilte heliribaga.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="Kdenlive'i helimikser" >}}

Probleemide lahendamise poole pealt pakub Kdenlive rohkeid jõudluse ja
kasutatavuse täiustusi. Samuti on arendajad parandanud palju vigu
videoredaktori Windowsi versioonis.

## [Dolphin](https://userbase.kde.org/Dolphin)i eelvaatlused ja liikumine

KDE võimas failisirvija [Dolphin](https://userbase.kde.org/Dolphin) pakub
uusi võimalusi leida ja kasutada kõike, mida süsteem pakub. Üks sellisi
võimalusi on põhjalikuma otsingu valikute ümberkujundamine, teine uuendus
lubab aga liikuda külastatud asukohtade ajaloos pikalt edasi ja tagasi, kui
hoida all tööriistariba nooleikooni. Võimalus kiiresti jõuda viimati
salvestatud või viimati kasutatud failide juurde on samuti ümber töötatud ja
pisitõrked kõrvaldatud.

Üks Dolphini arendajate põhimuresid on olnud anda kasutajale võimalus näha
faili sisu enne selle avamist. Dolphini uues versioonis saab GIF-i
eelvaatlust näha, kui tõsta see esile ja siis viia hiirekursor
eelvaatluspaneeli kohale. Seal saab ka klõpsuga esitada video- ja helifaile.

Kui oled koomiksisõber, siis võib Dolphin nüüd näidata .cb7-vormingus koomikseid (vaata selle koomiksivormingu toetuse kohta ka allpool Okulari juures). Ja kui juba pisipiltidest juttu tuli, siis juba pikemat aega on Dolphinis olnud võimalus neid suurendada-vähendada: hoia all klahvi <kbd>Ctrl</kbd>, liiguta hiireratast ning pisipilt suurenebki või kahaneb. Uus asi selle juures on see, et nüüd saab suurenduse-vähenduse lähtestada vaiketasemele kiirklahviga <kbd>Ctrl</kbd> + <kbd>0</kbd>.

Veel üks kasulik omadus on see, et Dolphin näitab nüüd, millised programmid
takistavad seadme lahutamist.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): telefoniga töölaual tegutsema

[KDE Connect](https://community.kde.org/KDEConnect)i uusim versioon pakub
mitmeid uus võimalusi. Üks tähelepanuväärsemaid on uus SMS-i rakendus, mis
lisaks SMS-ide lugemisele ja kirjutamisele võib näidata ka kogu vestluste
ajalugu.

Uus [Kirigami](https://kde.org/products/kirigami/)-põhine kasutajaliides
lubab lisaks Androidile KDE Connecti kasutada ka teistel mobiilse Linuxi
platvormidel, mida pakuvad uued seadmed, näiteks PinePhone ja Librem
5. Kirigami liides pakub uusi võimalusi ka sel juhul, kui suhtlevad kaks
töölauda omavahel, näiteks meedia juhtimine, kaugsisend, seadmesse
helistamine, failide ülekanne ja käskude käivitamine.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="Töölaua uus liides" >}}

Juba varem sai KDE Connecti abil juhtida meediafailide esitamist töölaual,
aga nüüd saab telefonist juhtida ka süsteemi üldist helitugevust, mis on
eriti mugav, kui soovid kasutada telefoni umbes nagu teleripulti. Esinedes
aga saab KDE Connecti abil juhtuda oma esitlust slaidide vahel edasi-tagasi
liikudes.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Vee üks võimalus, mis on samuti juba pikemat aega olemas, on faili saatmine
KDE Connecti abil telefoni, aga nüüdses versioonis saab telefon faili kohe
kättesaamisel avada. KDE itineraar näiteks saab niimoodi saata reisiteavet
Kmailist otse telefoni.

Muide, lisaks saab nüüd saata faile ka Thunarist (Xfce failihaldur) ja
Elementary rakendustest, näiteks Pantheon Filesist. Mitme faili kopeerimise
edenemise näitamist on tublisti täiustatud ning Androidist saadud failid
näitavad viimaks õiget muutmisaega.

Mis puudutab märguandeid, siis nüüd saab Androidi märguannete toiminguid
käivitada otse töölaualt ning senisest kvaliteetsemate märguannete
esitamiseks tarvitab KDE Connect Plasma märguandekeskuse mitmekesiseid
võimalusi. Paardumise kinnituse märguanne enam ei aegu, nii et seda ei saa
enam tähele panemata jätta.

Ning lõpetuseks: kui kasutad KDE Connecti käsurealt, siis nüüd on
*kdeconnect-cli* saanud palju täiuslikuma *zsh* automaatse lõpetamise.

## Võrgupildid ja [Gwenview](https://userbase.kde.org/Gwenview)

[Gwenview](https://userbase.kde.org/Gwenview) võimaldab nüüd muudetud
piltide salvestamisel kohandada JPE tihenduse taset. Samuti on arendajad
parandanud jõudlust võrgupiltide kasutamise korral ning Gwenview suudab nüüd
võrgukohtadest pilte nii eksportida kui ka neisse pilte importida.

## [Okular](https://kde.org/applications/office/org.kde.okular) pakub .cb7 toetust

Dokumendinäitaja
[Okular](https://kde.org/applications/office/org.kde.okular), mis lubab
lugeda, annoteerida ja verifitseerida kõikvõimalikke dokumente, sealhulgas
PDF-, EPub-, ODT- ja MD-dokumente, toetab nüüd lisaks .cb7 vormingus
koomikseid.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): lihtne, aga igati täiuslik muusikamängija

[Elisa](https://kde.org/applications/multimedia/org.kde.elisa) on KDE
muusikamängija, mis ühendab endast lihtsuse elegantse ja moodsa
liidesega. Nüüdses versioonis on Elisa optimeerinud oma väljanägemist, et
tulla paremini toime kõrge DPI-ga ekraanidel. Samuti on see paremini
lõimitud teiste KDE rakendustega ning tarvitab nüüd KDE üleüldist
menüüsüsteemi.

Muusikafailide indekseerimist on täiustatud ning Elisa toetab nüüd
veebiraadioid ja isegi on pannud proovimiseks kaasa mõned jaamad.

{{< img src="elisa.png" caption="Elisa uuuendatud liides" >}}

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) on nüüd puutetundlik

KDE vaikimisi ekraanipiltide tegemise rakendus
[Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) on
veel üks KDE rakendus, mis on omandanud puutetundlikkuse. Selle uus
puutetundlik lohistamise käitleja muudab senisest veel palju lihtsamaks
ristkülikute joonistamise puutetundlike seadmete töölaual, mille põhjal
ekraanipilte teha.

Teiste täiustuste seas võib ära märkida uut automaatse salvestamise
võimalust, mis laseb mugavalt teha kiiresti järjest mitmeid ekraanipilte, ja
animeeritud edenemisribasid, mis näitavad eksimatu selgusega, mille kallal
on Spectacle parajasti ametis.

{{<img style="max-width:500px" src="spectacle.png" caption="Spectacle uus lohistamisvõimalus" >}}

## [Plasma lõimimine veebilehitsejaga](https://community.kde.org/Plasma/Browser_Integration) pakub mõningaid täiustusi

[Plasma veebilehitsejaga
lõimimine](https://community.kde.org/Plasma/Browser_Integration) pakub nüüd
võimalust koostada meedia juhtelementide tarbeks must nimekiri. See on abiks
siis, kui satud saidile, kus on rohkelt esitatavat meediat, mis muudab
raskeks valida, mida siis ikka töölaualt juhtida. Musta nimekirja lisamise
järel ei saa sellised saidid enam sinu meedia juhtelementidele reageerida.

Uus versioon lubab samuti alles hoida algse URL-i faili metaandmetes ja
lisab Web Share API toetuse. Viimane võimaldab veebilehitsejatel jagada
linke, teksti ja faile teiste rakendustega samamoodi, nagu seda teevad juba
pikka aega KDE rakendused omavahel, mis parandab KDE-väliste
veebilehitsejate, näiteks Firefoxi, Chrome/Chromiumi ja Vivaldi, lõimitust
KDE rakendustega.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Microsoft Store

Üsna mitmeid KDE rakendusi saab alla laadida Microsoft Store'ist. Krita ja
Okular on seal olnud juba ammu, nüüd liitus nendega kasutajasõbralik LaTeXi
redaktor [Kile](https://kde.org/applications/office/org.kde.kile).

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Uus

Nüüd jõuame uute lisanduste juurde:
[SubtitleComposer](https://invent. kde.org/kde/subtitlecomposer) ehk
rakendus, mis lubab hõlpsasti luua videole subtiitreid, on juba KDE
inkubaatoris, niisiis teel KDE rakenduste perekonna täisõiguslikuks
liikmeks. Tere tulemast!

Samuti on [plasma-nano](https://invent.kde.org/kde/plasma-nano), Plasma
sardseadmetele mõeldud minimalistlik versioon, liigutatud viimaks Plasma
hoidlasse, kus see ootab nüüd väljalaske 5.18 ilmumist.

## Väljalase 19.2

Mõned meie rakendused lasevad uusi versioone välja oma ajakava järgi, mõned
ilmuvad paljukesi korraga. Täna nägi ilmavalgust projektikimp 19.12, mis
varsti peaks jõudma ka rakenduste hoidlatesse ja distributsioonidesse. Vt
[väljalasete 19.12
lehekülge](https://www.kde.org/announcements/releases/19.12). Varem nimetati
seda kimpu üldnimetusega KDE rakendused, aga see muudeti
väljalasketeenuseks, et vältida segaduse tekkimist kõigi teiste KDE
rakendustega, aga ka seepärast, et see pole tegelikult üks tervik, vaid
koosneb kümnetest eri rakendustest.
