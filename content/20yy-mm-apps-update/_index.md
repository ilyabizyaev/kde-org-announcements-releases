---
title: KDE's MONTH 2020 Apps Update

publishDate: 2099-12-00 16:00:00

layout: page # don't translate

summary: "What Happened in KDE's Applications This Month"

type: announcement # don't translate
draft: true
---

git diff 'HEAD@{1 month ago}' HEAD

ssh ftpadmin@master.kde.org 'find stable/ -mtime 31'

https://mail.kde.org/pipermail/kde-announce-apps/

https://mail.kde.org/pipermail/kde-announce/

Intro Paragraph

# New releases

## Foo 1.0

Text

# Incoming

Stuff that passed incubator or kdereview or got a beta release or newly created projects.

# App Store

App store of the month review, Snap, F-Droid, Google Play, Microsoft, Mac App Store, Flathub, Appimagehub etc

# Website Updates

Any new websites this month?

# Releases 19.12.2

Some of our projects release on their own timescale and some get released en-masse. The 19.12.2 bundle of projects was released today and should be available through app stores and distros soon. See the [19.12.2 releases page](https://www.kde.org/info/releases-19.12.2.php) for details. This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because there are dozens of different products rather than a single whole.

Some of the fixes included in this release are:

* The [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) music player now handles files that do not contain any metadata
* Attachments saved in the *Draft* folder no longer disappear when reopening a message for editing in [KMail](https://kde.org/applications/internet/org.kde.kmail2)
* A timing issue that could cause the [Okular](https://kde.org/applications/office/org.kde.okular) document viewer to stop rendering has been fixed
* The [Umbrello](https://umbrello.kde.org/) UML designer now comes with an improved Java import

[19.12.2 release notes](https://community.kde.org/Releases/19.12_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;  [19.12.1 source info page](https://kde.org/info/releases-19.12.1) &bull; [19.12.1 full changelog](https://kde.org/announcements/changelog-releases.php?version=19.12.1)

# Stores

KDE software is available on many platforms and software app stores.

[![Snapcraft](../platforms/snapcraft.png)](https://snapcraft.io/publisher/kde)
[![Flathub](../platforms/flathub.png)](https://flathub.org/apps/search/kde)
[![Microsoft Store](../platforms/microsoft.png)](https://www.microsoft.com/en-gb/search?q=kde)
