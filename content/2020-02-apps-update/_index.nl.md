---
titel: Update van toepassingen van KDE in februari 2020

publishDate: 2020-02-06 12:00:00

layout: page # don't translate

samenvatting: "Wat gebeurde er in de toepassingen van KDE deze maand"

type: announcement # don't translate
---

Deze maand ziet een overvloed aan uitgaven met gerepareerde bugs. Terwijl we
allemaal van nieuwe functies houden, weten we dat mensen het fijn vinden
wanneer vergissingen en crashes opgeruimd en ook opgelost worden!

# Nieuwe uitgaven

## KDevelop 5.5

De grote vrijgave komt deze maand van [KDevelop
5.5](https://www.kdevelop.org/news/kdevelop-550-released), de IDE die het
schrijven van programma's in C++, Python en PHP gemakkelijker maken. We
hebben een flink aantal bugs gerepareerd en heel wat code van KDevelop
opgekapt. We hebben Lambda init captures in C++ toegevoegd, configureerbare
voorgedefinieerde selecties van controlesets zijn er nu in voor Clazy-tidy
en Clang, en codeaanvullen bij look-ahead probeert nu harder. PHP kreeg van
type voorziene eigenschappen van PHP 7.4 en ondersteuning is toegevoegd voor
array van type en klasse constante zichtbaarheid. In Python is nu
ondersteuning toegevoegd voor Python 3.8.

KDevelop is beschikbaar uit uw Linux distributie of als een AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) is onze TODO-lijst volger die integreert
met Kontact. Recente uitgaven van Kontact hadden een gebroken Zanshin en
gebruikers konden niet zien welke taken als volgende moesten worden
uitgevoerd. Maar deze maand is er een uitgave om [het weer opnieuw werkend
te krijgen](https://jriddell.org/2020/01/14/zanshin-0-5-71/). We kunnen nu
allen weer gemakkelijk de taken vinden waarmee we verder moeten gaan!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

[Okteta](https://kde.org/applications/utilities/org.kde.okteta), De
Hex-bewerker van KDE, had een uitgave met gerepareerde bugs en bevat een
nieuwe functie: een CRC-64 algoritme voor het hulpmiddel controlesom. Okteta
werkt ook de code bij voor het nieuwe Qt en KDE Frameworks.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

[KMyMoney](https://kmymoney.org/), de KDE toepassing die u helpt om uw
financieën te beheren, [bevatte verschillende reparaties van
bugs](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) en
een verbetering die ondersteuning toevoegde voor cheques met een
splitsingsprotocol

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Inkomend

Keysmith is een twee-factoren codegenerator voor Plasma Mobile en Plasma
bureaublad die de oath-toolkit gebruikt. De gebruikersinterface is
geschreven in de Kirigami, waarmee het er goed uitziet op elk
schermafmeting. Verwacht de uitgave spoedig.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) is een pakketbeheerder voor het Windows
besturingssysteem. Chocolatey houdt zich bezig met installeren en bijwerken
van alle software op het systeem en kan het leven gladder maken voor Windows
gebruikers.

[KDE heeft een account op Chocolatey](https://chocolatey.org/profiles/KDE)
en u kunt Digikam, Krita, KDevelop en Kate ermee installeren. Installatie is
erg gemakkelijk: Alle wat u hoeft te doen is `choco install kdevelop` en
Chocolatey zal de rest doen. Onderhouders van Chocolatey hebben hoge
standaarden, u kunt dus zeker zijn dat de pakketten getest en veilig zijn.

Als u een onderhouder bent van een KDE toepassing, overweeg dan of uw
toepassing toegevoegd zou moeten worden en of als u een uitrolbeheerder bent
van een grote hoeveelheid Windows systemen, bekijk dit dan eens voor het
installeren.

# Bijgewerkte website

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}
Ons webteam gaat door met het bijwerken van onze online aanwezigheid en heeft recent de [KMyMoney website](https://kmymoney.org) vernieuwd.

# Uitgave van 19.12.2

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en
sommigen in massa vrijgegeven. De bundel 19.12.2 van projecten is vandaag
vrijgegeven en zouden spoedig beschikbaar moeten zijn via opslagruimten voor
toepassingen en distributies. Zie de [19.12.2
uitgavepagina](https://www.kde.org/info/releases-19.12.2.php) voor
details. Deze bundel was eerder genaamd KDE Applications maar heeft geen
naam meer om een uitgaveservice te worden om verwarring te voorkomen met
alle andere toepassingen door KDE en omdat het tientallen verschillende
producten zijn in plaats van een enkel geheel.

Enige van de reparaties ingevoegd in deze uitgave zijn:

* De muziekspeler
  [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) behandelt
  nu bestanden die niets aan metagegevens bevatten
* Bijlagen opgeslagen in de map *Concepten* verdwijnen niet langer bij
  opnieuw openen van een bericht voor bewerken in
  [KMail](https://kde.org/applications/internet/org.kde.kmail2)
* Een probleem met timing die de oorzaak in de documentviewer
  [Okular](https://kde.org/applications/office/org.kde.okular) kan zijn van
  het stoppen van weergeven is gerepareerd
* De [Umbrello](https://umbrello.kde.org/) UML-ontwerper komt nu met een
  verbeterd importeren van Java

[19.12.2 release
notes](https://community.kde.org/Releases/19.12_Release_Notes) &bull;
[Wikipagina voor downloaden
pakket](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [19.12.2 informatiepagina
broncode](https://kde.org/info/releases-19.12.2) &bull; [19.12.2 volledige
log met
wijzigingen](https://kde.org/announcements/changelog-releases.php?version=19.12.2)
