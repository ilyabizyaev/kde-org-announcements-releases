---
title: Actualização de Fevereiro 2020 das Aplicações do KDE

publishDate: 2020-02-06 12:00:00

layout: page # don't translate

summary: "O que se Passou nas Aplicações do KDE Neste Mês"

type: announcement # don't translate
---

Este mês tem uma grande quantidade de lançamentos de correcções de
erros. Embora todos nós gostemos de funcionalidades novas, sabemos que as
pessoas também gostam quando os erros e estoiros são resolvidos!

# Novos lançamentos

## KDevelop 5.5

O grande lançamento deste mês conta com o [KDevelop
5.5](https://www.kdevelop.org/news/kdevelop-550-released), o IDE que torna a
criação de programas em C++, Python e PHP mais imples. Corrigimos uma grande
quantidade de erros e arrumámos boa parte do código do KDevelop. Adicionámos
as capturas de inicializações em Lambda no C++, existindo agora selecções de
configurações predefinidas no Clazy-tidy e no Clang; por outro lado, a
completação de código agora é mais abrangente. O PHP recebeu o suporte para
propriedades com tipos do PHP 7.4 e foi adicionado o suporte para lista de
tipos e para a visibilidade de constantes das classes. No Python, foi
adicionado o suporte para o Python 3.8.

O KDevelop está disponível na sua distribuição de Linux ou como uma
AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

O [Zanshin](https://zanshin.kde.org/) é o nosso gestor de listas de tarefas
que se integra com o Kontact. As versões mais recentes do Kontact
danificaram o Zanshin e os utilizadores ficaram sem saber quais as tarefas a
executar a seguir. Contudo, neste mês foi feito um novo lançamento para
[deixá-lo a funcionar de
novo](https://jriddell.org/2020/01/14/zanshin-0-5-71/). Agora podemos
facilmente encontrar as tarefas que temos de fazer!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

O [Okteta](https://kde.org/applications/utilities/org.kde.okteta), o editor
em hexadecimal do KDE, teve o lançamento de uma nova versão com correcções
de erros e que inclui uma nova funcionalidade: um algoritmo de CRC-64 para a
ferramenta de códigos de validação. O Okteta também actualiza o código para
as novas versões do Qt e das Plataformas do KDE.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

O [KMyMoney](https://kmymoney.org/), a aplicação do KDE que o ajuda a gerir
as suas finanças, [incluiu várias correcções de
erros](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) e
uma melhoria que adicionou o suporte para formulários de cheques com o
protocolo de parcelas

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Recebido

O Keysmith é um gerador de códigos de autenticação em dois-factores para o
Plasma Mobile e o Plasma normal que usa o oath-toolkit. A interface de
utilizador foi criada com o Kirigami, tornando-o adaptável para qualquer
tamanho de ecrã. Espere novidades em breve.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

O [Chocolatey](https://chocolatey.org) é um gestor de pacotes para o sistema
operativo Windows. O Chocolatey lida com a instalação e actualização de
todas as aplicações no sistema e torna a vida mais fácil para os
utilizadores do Windows.

[O KDE tem uma conta no Chocolatey](https://chocolatey.org/profiles/KDE) e
você poderá instalar o Digikam, o Krita, o KDevelop e o Kate com ele. A
instalação é muito simples: Tudo o que precisa de fazer é `choco install
kdevelop` para que o Chocolatey faça o resto. Os responsáveis de manutenção
do Chocolatey têm padrões elevados, pelo que poderá ter a garantia de que os
pacotes foram testados e são seguros.

Se for o responsável de manutenção por alguma aplicação do KDE, pense em ter
a sua aplicação adicionada e, caso seja um administrador de sistemas Windows
em grande escala, pense de facto nesta solução nas suas instalações.

# Actualizações da Página Web

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}
A nossa equipa da Web continua a actualizar a nossa presença 'online' e remodelou recentemente a [página Web do KMyMoney](https://kmymoney.org).

# Versões 19.12.2

Alguns dos nossos projectos são lançados de acordo com as suas próprias
agendas e alguns são lançados em massa. A versão 19.12.2 dos projectos foi
lançada hoje e deverá estar disponível através das lojas de aplicações e
distribuições em breve. Consulte a [página da versão
19.12.2](https://www.kde.org/info/releases-19.12.2.php).  Este pacote foi
chamado anteriormente de Aplicações do KDE mas tem vindo a perder a sua
marca para se tornar um serviço de lançamento de versões, para evitar
confusões com todas as outras aplicações do Kde e porque consiste em dezenas
de diferentes produtos em vez de um único grupo.

Algumas das correcções incluídas nesta versão são:

* O leitor de músicas
  [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) agora lida
  com ficheiros que não possuem quaisquer meta-dados.
* Os anexos gravados na pasta *Rascunhos* não desaparecem mais ao reabrir
  uma mensagem para edição no
  [KMail](https://kde.org/applications/internet/org.kde.kmail2)
* Foi corrigido um problema de sincronização que fazia com que o
  visualizador de documentos
  [Okular](https://kde.org/applications/office/org.kde.okular) parasse de
  desenhar
* O desenhador de UML [Umbrello](https://umbrello.kde.org/) vem agora com
  uma importação melhorada de código Java

+ [Notas da versão
19.12.2](https://community.kde.org/Releases/19.12_Release_Notes) para mais
informações sobre pacotes e problemas conhecidos.  &bull; [Página de
transferência de pacotes da
Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Página de informação do código do 19.12
RC](https://kde.org/info/applications-19.12.2) &bull; [Registo de alterações
completo do
19.12.2](https://kde.org/announcements/changelog-releases.php?version=19.12.2)
