---
title: KDE:s programuppdateringar i februari 2020

publishDate: 2020-02-06 12:00:00

layout: page # don't translate

summary: "Vad har hänt i KDE:s program den här månaden"

type: announcement # don't translate
---

Den här månaden har vi en uppsjö av utgåvor med felrättningar. Fastän vi
alla gillar nya funktioner, vet vi att folk älskar när misstag och kraschar
rensas bort och dessutom löses!

# Nya utgåvor

## KDevelop 5.5

Den stora utgåvan den här månaden kommer från [KDevelop
5.5](https://www.kdevelop.org/news/kdevelop-550-released), den integrerade
utvecklingsmiljön som gör det enklare att skriva program i C++, Python och
PHP. Vi har rättat en massa fel och rensat upp en hel del av koden i
KDevelop. Vi har lagt till Lambda initeringsinfångningar i C++,
inställningsbara fördefinierade kontrollmängdsval finns ny för Clazy-tidy
och Clang, och framåtriktad komplettering försöker längre. PHP får typade
egenskaper från PHP 7.4, och stöd har lagts till för fält av typer och
synlighet för klasskonstanter. När det gäller Python, har nu stöd för Python
3.8 lagts till.

KDevelop är tillgänglig från din Linux-distribution eller som en AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) är vår uppföljare av uppgiftslistor, som
är integrerad med Kontact. Senare utgåvor av Kontact hade gjort att Zanshin
slutade fungera, och användare lämnades utan att veta vilka uppgifter som
skulle utföras härnäst. Men den här månaden har en utgåva gjorts för [att få
den att fungera igen](https://jriddell.org/2020/01/14/zanshin-0-5-71/). Nu
kan vi alla enkelt hitta uppgifterna som vi måste ta tag i!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

För [Okteta](https://kde.org/applications/utilities/org.kde.okteta), KDE:s
hexadecimaleditor, har en felrättningsutgåva gjorts som inkluderar en ny
funktion: en CRC-64 algoritm för checksummeverktyget. Okteta har också
uppdaterat koden för nya Qt- och KDE-ramverk.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

[KMyMoney](https://kmymoney.org/), KDE-programmet som hjälper dig hantera
din ekonomi [inkluderar flera
felrättningar](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html)
och en förbättring som lägger till stöd för checkformulär med delat
protokoll.

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Inkommande

Keysmith är en twåfaktors kodgenerator för Plasma-mobil och Plasma-skrivbord
som använder oath-toolkit. Användargränssnittet är skrivet med Kirigami,
vilket gör det smidigt med alla skärmstorlekar. Förvänta dig utgåvor snart.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) är en pakethanterare för
operativsystemet Windows. Chocolatey hanterar att installera och uppdatera
programvara på systemet och kan förenkla livet för Windows-användare.

[KDE har ett konto på Chocolatey](https://chocolatey.org/profiles/KDE) och
du kan installera Digikam, Krita, KDevelop och Kate via det. Installation är
mycket enkel: Allt du behöver göra är `choco install kdevelop` så gör
Chocolatey resten. Utvecklarna av Chocolatey har höga krav, så du kan vara
säker på att paketen är utprovade och säkra.

Om du är underhållsansvarig för ett KDE-program, fundera på att få ditt
program tillagt, och du är en stor uppdateringsledare för Windows, fungera
på att använda det för dina installationer.

# Webbplatsuppdateringar

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}
Vår webbgrupp fortsätter att uppdatera vår närvaro på nätet och har nyligen uppdaterat [KMyMoneys webbplats](https://kmymoney.org).

# Utgåvor 19.12.2

Några av våra projekt ges ut med egna tidplaner och vissa ges ut
tillsammans. Projekten i 19.12.2-paketet gavs ut idag och bör snart vara
tillgängliga via programbutiker och distributioner. Se [utgivningssidan för
19.12.2](https://www.kde.org/info/releases-19.12.2.php). Paketet kallades
tidigare KDE Applications, men märkningen har tagits bort så att det nu är
en utgivningstjänst för att undvika sammanblandning med alla andra program
av KDE, och eftersom det är dussintals olika produkter istället för en
helhet.

Några av felrättningarna som ingår i den här utgåvan är:

* Musikspelaren
  [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) hanterar nu
  filer som inte innehåller någon metadata
* Bilagor som sparas i korgen *Utkast* försvinner inte längre när ett brev
  öppnas igen för redigering i
  [KMail](https://kde.org/applications/internet/org.kde.kmail2)
* Ett tidrelaterat problem som kunde göra att återgivning i dokumentvisaren
  [Okular](https://kde.org/applications/office/org.kde.okular) stoppade har
  rättats.
* UML-konstruktionsprogrammet [Umbrello](https://umbrello.kde.org/)
  levereras nu med förbättrad import av Java

[19.12.2
versionsfakta](https://community.kde.org/Releases/19.12_Release_Notes)
&bull; [Wikisida för
paketnerladdning](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [19.12.2
källkodsinformationssida](https://kde.org/info/releases-19.12.2) &bull;
[19.12.2 fullständig
ändringslogg](https://kde.org/announcements/changelog-releases.php?version=19.12.2)
