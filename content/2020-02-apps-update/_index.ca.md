---
title: Actualització de les aplicacions del KDE de febrer de 2020

publishDate: 2020-02-06 12:00:00

layout: page # don't translate

summary: "Què ha passat a les aplicacions del KDE aquest mes"

type: announcement # don't translate
---

Aquest mes ha vist una plètora de llançaments de correccions
d'errors. Encara que a tothom li agraden les funcionalitats noves, també
sabem que a la gent li agrada que els errors i les fallades s'endrecin i es
resolguin.

# Llançaments nous

## KDevelop 5.5

El llançament gran d'aquest mes correspon al [KDevelop
5.5](https://www.kdevelop.org/news/kdevelop-550-released), l'IDE que
facilita escriure programes en C++, Python i PHP. S'han corregit una pila
d'errors i s'ha endreçat molt el codi del KDevelop. S'han afegit les
captures «init» de Lambda al C++, les seleccions de conjunts de
comprovacions predefinides que es poden configurar ara són per al Clazy-tidy
i el Clang, i la compleció de cerca endavant ara ho intenta més bé. El PHP
ha aconseguit les propietats de tipus del PHP 7.4 i s'ha afegit la
implementació per a les matrius de visibilitat constant de tipus i
classe. Al Python ara s'ha afegit la implementació per al Python 3.8.

El KDevelop és disponible a la vostra distribució de Linux o com una
AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

El [Zanshin](https://zanshin.kde.org/) és el seguidor de la llista de
tasques pendents que s'integra amb el Kontact. Les publicacions recents del
Kontact havien trencat el Zanshin i els usuaris no podin conèixer les
tasques que s'apropaven. Però aquest mes, s'ha efectuat una publicació per a
que [torni a
funcionar](https://jriddell.org/2020/01/14/zanshin-0-5-71/). Ara es poden
trobar fàcilment les tasques que cal portar a terme!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

L'[Okteta](https://kde.org/applications/utilities/org.kde.okteta), l'editor
hexadecimal del KDE, ha publicat una versió de correcció d'errors i inclou
una funcionalitat nova: un algorisme CRC-64 per a l'eina de suma de
verificació. L'Okteta també actualitza el codi per adaptar-se a les Qt i els
Frameworks nous del KDE.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

El [KMyMoney](https://kmymoney.org/), l'aplicació del KDE que ajuda a
gestionar les finances, [inclou diverses correccions
d'errors](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html)
i una millora per permetre formularis de xecs amb protocol de desglossament

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Entrades

El Keysmith és un generador de codi de dos factors per al Plasma Mobile i el
Plasma Desktop que usa «oath-toolkit». La interfície d'usuari està escrita
en el Kirigami, fent que s'adapti a qualsevol mida de pantalla. S'espera una
publicació aviat.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

El [Chocolatey](https://chocolatey.org/) és un gestor de paquets per al
sistema operatiu Windows. El Chocolatey gestiona la instal·lació i
l'actualització de tot el programari del sistema i pot facilitar el
manteniment per als usuaris del Windows.

El [KDE té un compte al Chocolatey](https://chocolatey.org/profiles/KDE) i
podeu instal·lar el Digikam, Krita, KDevelop i Kate a través d'ell. La
instal·lació és molt senzilla: només cal fer «choco install kdevelop» i el
Chocolatey farà la resta. Els mantenidors del Chocolatey tenen un estàndards
alts, i per tant, podeu tenir la seguretat que els paquets estan provats i
són segurs.

Si manteniu una aplicació KDE, considereu afegir-hi la vostra aplicació, i
si sou un gestor de desplegament a entorns Windows grans, tingueu-ho present
per a les instal·lacions.

# Actualitzacions del lloc web

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}
L'equip web continua actualitzant la nostra presència en línia i recentment ha actualitzat el [lloc web del KMyMoney](https://kmymoney.org).

# Llançaments 19.12.2

Varis dels nostres projectes publiquen en la seva pròpia escala de temps i
altres es publiquen conjuntament. El paquet de projectes 19.12.2 s'ha
publicat avui i aviat hauria de ser disponible a través de les botigues
d'aplicacions i de les distribucions. Vegeu la [pàgina del llançament
19.12.2](https://www.kde.org/info/releases-19.12.2.php) per als
detalls. Aquest paquet s'havia anomenat prèviament Aplicacions del KDE però
s'ha descatalogat per esdevenir un servei de llançaments per evitar
confusions amb totes les altres aplicacions del KDE i perquè són una dotzena
de productes diferents en lloc d'un únic grup.

Algunes de les correccions incloses en aquest llançament són:

* El reproductor musical
  [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) ara
  gestiona fitxers que no contenen cap metadada
* Els adjunts desats a la carpeta *Esborrany* ja no desapareixen en reobrir
  una missatge per a editar-lo al
  [KMail](https://kde.org/applications/internet/org.kde.kmail2)
* S'ha corregit un problema de temps que podria provocar que el
  visualitzador de documents
  [Okular](https://kde.org/applications/office/org.kde.okular) aturi la
  representació
* El dissenyador UML [Umbrello](https://umbrello.kde.org/) ara té una
  importació Java millorada

+ [Notes de llançament de la
19.12.2](https://community.kde.org/Releases/19.12_Release_Notes) &bull;
[Pàgina wiki de baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Pàgina d'informació del codi font de la
19.12.2](https://kde.org/info/releases-19.12.2) &bull; [Registre complet de
canvis de la
19.12.2](https://kde.org/announcements/changelog-releases.php?version=19.12.2)
per a informació quan als arxius tar i els problemes coneguts.+ [Pàgina wiki
de baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) +
[Pàgina d'informació del codi font de la 19.12
RC](https://kde.org/info/applications-19.11.90)
