---
title: Что нового в приложениях KDE в феврале 2020 года

publishDate: 2020-02-06 12:00:00 # don't translate

layout: page # don't translate

summary: "Что произошло в приложениях KDE за последний месяц"

type: announcement # don't translate
---

В этом месяце вышло множество выпусков приложений, содержащих исправления
ошибок. Хотя всем нравится добавление новые функций, точно также важно
устранять ошибки.

# Новые выпуски

## Среда разработки KDevelop 5.5

Это большое описание начинается с [KDevelop 5.5]
(https://www.kdevelop.org/news/kdevelop-550-released) — среды разработки
облегчающей написание программ на C ++, Python и PHP. В этом выпуске
исправно множество ошибок и внесено значительное количество изменений в код
KDevelop. Добавлен захват Lambda init в C ++, в Clazy-tidy и Clang доступны
настраиваемые предопределённые варианты выбора контрольных наборов, улучшено
автодополнение. В PHP добавлена поддержка типизированных свойств PHP 7.4,
добавлена поддержка массивов типов и видимости постоянных класса. В Python
добавлена поддержка Python 3.8.

Установите обновлённую версию KDevelop из пакетов своего дистрибутива или в
виде пакета Appimage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Управление задачами Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) — это приложение для отслеживания
выполнения списков задач, работающее совместно с электронным секретарём
Kontact. Недавние выпуски Kontact нарушили работоспособность Zanshin, но в
этом месяце было выпущено исправление, [восстанавливающее
работоспособность](https://jriddell.org/2020/01/14/zanshin-0-5-71/). Теперь
снова возможно легко найти задачи, которые ожидают выполнения.

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Панель Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## Среда разработки RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Шестнадцатеричный редактор Okteta 0.26.3

Новый выпуск шестнадцатеричного редактора
[Okteta](https://kde.org/applications/utilities/org.kde.okteta), содержит
исправления, а также в нём добавлена поддержка алгоритма CRC-64 в
инструменте подсчёта контрольных сумм. В этом выпуске также внесены
изменения для поддержки новых версий библиотек Qt и KDE Frameworks.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## Учёт финансов KMyMoney 5.0.8

Выпуск приложения для учёта финансов [KMyMoney](https://kmymoney.org/)
включает [исправления
ошибок](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) и
улучшенную поддержку форм чеков с разделением.

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Грядущие выпуски

Keysmith — приложение для Plasma Mobile и Plasma Desktop для создания
одноразовых кодов, используемых в качестве второго фактора авторизации,
использующее библиотеку oath-toolkit. Пользовательский интерфейс написан на
Kirigami, что позволяет использовать приложение на экранах любого
размера. Ожидайте выпуск этого приложения в ближайшее время.

# Система управления пакетами Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) — менеджер пакетов для
Windows. Chocolatey обеспечивает установку и обновление программного
обеспечения и может облегчить жизнь пользователям Windows.

[У KDE есть учётная запись в системе управления пакетами
Chocolatey](https://chocolatey.org/profiles/KDE), и с помощью этой системы
возможно установить Digikam, Krita, KDevelop и Kate. Установка очень проста:
все, что нужно сделать, это ввести команду «choco install kdevelop», а
Chocolatey сделает все остальное. Сопровождающие из команды Chocolatey
предъявляют  высокие требования, поэтому можно быть уверенным, что пакеты
проверены и безопасны.

Если вы являетесь сопровождающим приложения KDE, подумайте о том, чтобы
добавить своё приложение, а если вы являетесь ответственным за развёртывание
приложений в Windows, подумайте о начале использования Chocolatey.

# Обновления веб-сайта

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}
Команда разработчиков веб-сайтов продолжает работу и подготовила обновление сайта [приложения для учёта финансов KMyMoney](https://kmymoney.org).

# Выпуск 19.12.2

Некоторые из наших проектов делают выпуск по собственным календарным
графикам, а другие выпускаются массово. Сегодня был выпущен комплект
проектов версии 19.12.2, скоро их новые версии будут доступны в магазинах
приложений и через дистрибутивы. Подробную информацию смотрите на [странице
выпусков 19.12.2](https://www.kde.org/info/releases-19.12.2.php). Этот
комплект проектов ранее назывался «Приложения KDE» или «KDE Applications»,
но теперь этот бренд больше не используется, чтобы не путать с другими
приложениями от KDE и поскольку «KDE Applications» на самом деле
представляли собой десятки продуктов, а не один.

Некоторые из исправленных в этом выпуске ошибок:

* Музыкальный проигрыватель
  [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) теперь
  корректно обрабатывает файлы, не содержащие метаданных;
* Исправлена ошибка пропадания вложений при редактировании сообщения из
  папки *Черновики* в почтовом клиенте
  [KMail](https://kde.org/applications/internet/org.kde.kmail2);
* Исправлена ошибка синхронизации, которая могла привести к остановке вывода
  изображения в программе просмотра документов
  [Okular](https://kde.org/applications/office/org.kde.okular);
* В инструмент UML-моделирования [Umbrello](https://umbrello.kde.org/)
  улучшена поддержка  импорта модулей, написанных на языке программирования
  Java.

[Примечания к выпуску
19.12.2](https://community.kde.org/Releases/19.12_Release_Notes) &bull;
[Страница сведений о получении приложений базы знаний
пользователей](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Ссылки на исходные коды приложений выпуска
19.12.2](https://kde.org/info/releases-19.12.2) &bull; [Полный список
изменений выпуска
19.12.2](https://kde.org/announcements/changelog-releases.php?version=19.12.2)
