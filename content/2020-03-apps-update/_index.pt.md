---
title: Actualização de Março de 2020 das Aplicações do KDE

publishDate: 2020-03-05 15:50:00 # don't translate

layout: page # don't translate

summary: "O que se Passou nas Aplicações do KDE Neste Mês"

type: announcement # don't translate
---

## Novos lançamentos

### Choqok 1.7

Fevereiro começou uma actualização há muito esperada na nossa aplicação de
micro-blogs
[Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

As correcções importantes incluem o limite de 280 caracteres no Twitter, a
possibilidade de desactivar contas e o suporte para 'tweets' estendidos.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore e o Gestor de Partições do KDE 4.1.0

O nosso programa de formatação de discos Gestor de Partições teve um novo
lançamento com bastante trabalho feito na biblioteca KPMCore que, por sua
vez, também é usada no instalador de distribuições Calamares. A aplicação em
si adicionou o suporte para os sistemas de ficheiros Minix.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Gestor de Partições" >}}

### KPhotoAlbum 5.6

Se tiver centenas ou mesmo milhares de imagens no seu disco rígido, torna-se
impossível recordar a história por trás de cada imagem individual ou os
nomes das pessoas fotografadas. O KPhotoAlbum foi criado para o ajudar a
descrever as suas imagens e depois a procurar nesse grande grupo de imagens
de forma rápida e eficiente.

Esta versão traz em particular grandes melhorias de performance ao marcar um
grande grupo de imagens e melhorias de performance na área de
miniaturas. Muito obrigado ao Robert Krawitz por mais uma vez mergulhar no
código e procurar locais para optimizar e remover redundâncias!

Para além disso, esta versão adiciona o suporte para a plataforma de
'plugins' do KDE Purpose.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Recebido

O editor de marcas ID do MP3 passou para o 'kdereview', sendo o primeiro
passo para obter versões novas.

E o cliente de Rocket.chat Ruqola saiu do 'kdereview' e passou a estar
pronto para ser lançado. Contudo, segundo o responsável de manutenção
Laurent Montel, é necessário que o mesmo seja remodelado como elemento
gráfico, pelo que ainda há muito trabalho a fazer.

## Loja de Aplicações

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Uma aplicação nova na Loja da Microsoft para o Windows é o
[Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

Um leitor de música moderno e bonito feito com amor pelo KDE. Navegue e
toque a sua música com muita facilidade! O Elisa deverá conseguir ler muitos
dos formatos de ficheiros de música. O Elisa está feito para ser acessível
para todos.

Entretanto, o Digikam está pronto para ser submetido à Loja da Microsoft.

## Actualizações da Página Web

O [KDE Connect](https://kdeconnect.kde.org/), a nossa útil ferramenta de
integração do ambiente de trabalho com os telemóveis, tem uma nova página
Web.

O [Kid3](https://kid3.kde.org/) tem uma nova e bonita página Web com
notícias e ligações de transferência em todas as lojas onde está disponível.

## Versão 19.12.3

Alguns dos nossos projectos são lançados de acordo com as suas próprias
agendas e alguns são lançados em massa. A versão 19.12.3 dos projectos foi
lançada hoje e deverá estar disponível através das lojas de aplicações e
distribuições em breve. Consulte a [página da versão
19.12.3](https://www.kde.org/info/releases-19.12.3.php).  Este pacote foi
chamado anteriormente de Aplicações do KDE mas tem vindo a perder a sua
marca para se tornar um serviço de lançamento de versões, para evitar
confusões com todas as outras aplicações do Kde e porque consiste em dezenas
de diferentes produtos em vez de um único grupo.

Algumas das correcções incluídas nesta versão são:

* O suporte para os atributos de ficheiros para as partilhas SMB foi
  melhorado, com [diversas
  modificações](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)
* O K3b agora aceita os ficheiros WAV como ficheiros de áudio [ERRO
  399056](https://bugs.kde.org/show_bug.cgi?id=399056)
  [Commit](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)
* O Gwenview consegue agora carregar imagens remotas por HTTPS
  [Modificação](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [Notas da versão
19.12.3](https://community.kde.org/Releases/19.12_Release_Notes) para mais
informações sobre pacotes e problemas conhecidos.  💻 [Página de
transferência de pacotes da
Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)  💻
[Página de informação do código do
19.12.3](https://kde.org/info/applications-19.12.3) 💻 [Registo de alterações
completo do
19.12.3](https://kde.org/announcements/changelog-releases.php?version=19.12.3)
💻
