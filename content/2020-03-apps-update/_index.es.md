---
title: Actualización de las Aplicaciones de KDE de marzo de 2020

publishDate: 2020-03-05 15:50:00 # don't translate

layout: page # don't translate

summary: "¿Qué ha ocurrido este mes en las Aplicaciones de KDE?"

type: announcement # don't translate
---

## Nuevos lanzamientos

### Choqok 1.7

Febrero ha comenzado con una largamente esperada actualización de nuestra
aplicación de microblogueo
[Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

Las correcciones importantes incluyen el límite de 280 caracteres en
Twitter, la posibilidad de desactivar cuentas y el uso de tuits extendidos.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore y el gestor de particiones de KDE 4.1.0

La nueva versión de nuestro programa de formateo de disco, el «Gestor de
particiones», llega con gran cantidad de trabajo invertido en la biblioteca
KPMCore, que también usa el instalador de distribuciones Calamares. La
propia aplicación permite usar ahora el sistema de archivos Minix.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="El gestor de particiones" >}}

### KPhotoAlbum 5.6

Si tiene cientos o tal vez miles de imágenes en su disco duro, le resultará
imposible recordar la historia que hay detrás de cada una de las imágenes o
los nombres de las personas que aparecen en ellas. KPhotoAlbum se creó para
ayudarle a describir sus imágenes y poder realizar búsquedas en ellas de
forma rápida y eficiente.

En particular, este lanzamiento proporciona grandes mejoras de rendimiento
al etiquetar un gran número de imágenes y también en la vista de
miniaturas. Nuestro agradecimiento a Robert Krawitz por volver a bucear en
el código fuente y encontrar los puntos que se podían optimizar y eliminar
redundancias.

Además, este lanzamiento añade el uso del complemento «Purpose» de KDE
Frameworks.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Novedades

Kid3, el editor de etiquetas de MP3, se ha movido a kdereview, el primer
paso para producir nuevos lanzamientos.

Y la aplicación de Rocket.chat, Ruqola, ha salido de kdereview y ya está
lista para un próximo lanzamiento. No obstante, según las palabras de su
encargado Laurent Montel, está planeada su reescritura con widgets de Qt,
por lo que aún falta bastante trabajo por realizar.

## Tienda de aplicaciones

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Una novedad en la tienda de aplicaciones de Microsoft Windows es
[Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

Un moderno y bonito reproductor de música creado con todo el amor de
KDE. ¡Explore y reproduzca su música de un modo fácil! Elisa debería poder
leer casi todos los tipos de archivos de música. Elisa se ha creado con la
idea de ser accesible para todos.

Mientras tanto, Digikam está listo para enviar a la tienda de aplicaciones
de Microsoft.

## Actualización del sitio web

[KDE Connect](https://kdeconnect.kde.org/), nuestra herramienta de
integración de teléfono móvil y escritorio, dispone de un nuevo sitio web.

[Kid3](https://kid3.kde.org/) dispone de un nuevo sitio web con noticias y
enlaces de descarga para todas las tiendas de aplicaciones en las que está
disponible.

## Lanzamiento de 19.12.3

Algunos de nuestros proyectos se publican según su propio calendario,
mientras que otros se publican en masa. El paquete de proyectos 19.12.3 se
ha publicado hoy y debería estar disponible próximamente en las tiendas de
aplicaciones y en las distribuciones. Consulte la [página de lanzamientos
19.12.3](https://www.kde.org/info/releases-19.12.3.php) para más
detalles. Este paquete se llamaba anteriormente Aplicaciones de KDE, pero se
ha simplificado su nombre para que se convierta en un servicio de
lanzamiento, evitando así confusiones con la totalidad de aplicaciones
creadas por KDE, ya que está compuesto de docenas de productos diferentes en
lugar de su totalidad.

Estas son algunas de las correcciones de errores que incluye este
lanzamiento:

* El uso de atributos de archivos en recursos compartidos SMB se ha
  mejorado. [Varias modificaciones de
  código](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)
* K3b acepta ahora archivos WAV como archivos de sonido. [Error
  399056](https://bugs.kde.org/show_bug.cgi?id=399056). [Código
  modificado](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d).
* Gwenview puede cargar ahora imágenes remotas mediante https. [Código
  modificado](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626).

💻 [Notas del lanzamiento de
19.12](https://community.kde.org/Releases/19.12_Release_Notes) para
información sobre los tarballs y los problemas conocidos. 💻 [Página wiki de
descarga de
paquetes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) 💻
[Página de información principal sobre
19.12.3](https://kde.org/info/releases-19.12.3) 💻 [Registro completo de
cambios
19.12.3](https://kde.org/announcements/changelog-releases.php?version=19.12.3)
💻
