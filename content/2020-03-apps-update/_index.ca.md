---
title: Actualització de les aplicacions del KDE de març de 2020

publishDate: 2020-03-05 15:50:00 # don't translate

layout: page # don't translate

summary: "Què ha passat a les aplicacions del KDE aquest mes"

type: announcement # don't translate
---

## Llançaments nous

### Choqok 1.7

El febrer ha començat amb una actualització llargament esperada de
l'aplicació de microblogs [Choqok]
(https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

Les esmenes importants inclouen el límit de 280 caràcters al Twitter,
permetre desactivar comptes i acceptar tuits ampliats.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore i Gestor de particions del KDE 4.1.0

El programa de formatat de disc Gestor de particions ha tret un llançament
nou i s'ha fet molta feina a la biblioteca del KPMCore que també s'utilitza
a l'instal·lador de la distribució Calamares. S'ha afegit el suport per al
sistema de fitxers Minix a la mateixa aplicació.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Gestor de particions" >}}

### KPhotoAlbum 5.6

Si teniu centenars o inclús milers d'imatges al disc dur, esdevé impossible
recordar la història que hi ha darrera de cada una de les imatges o el nom
de les persones fotografiades. El KPhotoAlbum s'ha creat per ajudar-vos a
descriure les imatges i després cercar ràpidament i eficientment dins la
gran col·lecció de fotografies.

En particular, aquest llançament aporta grans millores de rendiment en
etiquetar un gran nombre d'imatges i a la vista de miniatures. El nostre
agraïment al Robert Krawitz per tornar a submergir-se en el codi i trobar
les optimitzacions i eliminar les redundàncies!

Addicionalment, aquest llançament afegeix el suport per al marc de treball
del connector «purpose» del KDE.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Entrades

L'editor Kid3 d'etiquetes ID MP3 s'ha mogut a «kdereview», el primer pas per
fer nous llançaments.

I l'aplicació Ruqola del Rocket.chat ha passat pel «kdereview» i s'ha mogut
en preparació per al llançament. Tanmateix, el mantenidor Laurent Montel ha
indicat que s'ha planificat la reescriptura amb els Qt Widgets i encara hi
ha molta feina a fer.

## Botigues d'aplicacions

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

La novetat a la Microsoft Store per al Windows és
l'[Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

Un reproductor musical modern i bonic, fet a consciència per la Comunitat
KDE. Navegueu i reproduïu la música de manera fàcil! L'Elisa hauria de
llegir gairebé qualsevol fitxer de música. L'Elisa s'ha fet per a ser
accessible a tothom.

Mentrestant, el Digikam està preparat per enviar-se a la Microsoft Store.

## Actualitzacions del lloc web

El [KDE Connect](https://kdeconnect.kde.org/), l'eina d'integració del
telèfon i l'escriptori, té un lloc web nou.

El [Kid3](https://kid3.kde.org/) té un lloc web nou de trinca amb notícies i
enllaços de baixades a totes les botigues a on està disponible.

## Llançaments 19.12.3

Varis dels nostres projectes publiquen en la seva pròpia escala de temps i
altres es publiquen conjuntament. El paquet de projectes 19.12.3 s'ha
publicat avui i aviat hauria de ser disponible a través de les botigues
d'aplicacions i de les distribucions. Vegeu la [pàgina del llançament
19.12.3](https://www.kde.org/info/releases-19.12.3.php) per als
detalls. Aquest paquet s'havia anomenat prèviament Aplicacions del KDE però
s'ha descatalogat per esdevenir un servei de llançaments per evitar
confusions amb totes les altres aplicacions del KDE i perquè són una dotzena
de productes diferents en lloc d'un únic grup.

Algunes de les correccions incloses en aquest llançament són:

* Ha millorat el suport per als atributs de fitxer a les comparticions SMB,
  [diversos
  commits](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)
* Ara el K3b accepta fitxers WAV com a fitxers d'àudio. [Error
  399056](https://bugs.kde.org/show_bug.cgi?id=399056).
  [Commit](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)
* Ara el Gwenview pot carregar imatges remotes via https
  [Commit](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [Notes de llançament de la
19.12](https://community.kde.org/Releases/19.12_Release_Notes) per a
informació quant als arxius tar i els problemes coneguts. 💻 [Pàgina wiki de
baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) 💻
[Pàgina d'informació del codi font de la
19.12.3](https://kde.org/info/releases-19.12.3) 💻 [Registre complet de
canvis de la
19.12.3](https://kde.org/announcements/changelog-releases.php?version=19.12.3)
💻
