---
title: Atualização de março de 2020 dos aplicativos da KDE

publishDate: 2020-03-05 15:50:00 # don't translate

layout: page # don't translate

summary: "O que aconteceu com o KDE Applications este mês"

type: announcement # don't translate
---

## Novos lançamentos

### Choqok 1.7

Fevereiro começou com a aguardada atualização de nosso aplicativo de
microblogging
[Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

As correções importantes incluem o limite de 280 caracteres no Twitter,
permitir desabilitar contas e suporte a tweets estendidos.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore e KDE Partition Manager 4.1.0

Nosso programa de formatação Partition Manager lançou uma nova versão com
muito trabalho feito na biblioteca KPMCore que também é usada pelo
instalador de distribuições Calamares. O aplicativo em si adicionou o
suporte ao sistema de arquivos Minix.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Partition Manager" >}}

### KPhotoAlbum 5.6

Se você tem centenas ou milhares de imagens em seu disco rígido, se torna
impossível lembrar a história atrás de cada imagem ou o nome das pessoas
fotografadas. O KPhotoAlbum foi criado para ajudá-lo a descrever suas
imagens e então pesquisar na grande pilha de imagens de forma rápida e
eficiente.

Esta versão trás particularmente melhorias enormes na performance ao
etiquetar um grande número de imagens e melhorias na performance para a
visualização de miniaturas. Agradecemos novamente ao Robert Krawitz por
mergulhar no código e encontrar locais para otimização e remoção de
redundâncias!

Adicionalmente, esta versão adiciona o suporte à infraestrutura de plugin do
KDE purpose.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## O que está por vir

O editor de etiquetas ID de MP3 Kid3 foi movido para o kdereview, o primeiro
passo para lançar novas versões.

E o Ruqola, o aplicativo cliente do Rocket.chat, passou no kdereview e agora
está pronto para ser lançado. No entanto, segundo o mantenedor Laurent
Montel, o  aplicativo está sendo reescrito como widget, então há muito
trabalho a ser feito ainda.

## Loja de aplicativos

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Novo na Microsoft Store do Windows é o
[Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

Um reprodutor de músicas moderno e lindo feito com amor pela KDE. Navegue e
toque sua música facilmente. O Elisa deve conseguir ler quase qualquer tipo
de arquivo de música. O Elisa é feito para ser acessível a todos.

Enquanto isto o Digikam está pronto para ser enviado à Microsoft Store.

## Atualizações dos sites

O [KDE Connect](https://kdeconnect.kde.org/), nossa ferramenta bacana de
integração entre celular e área de trabalho ganhou um novo site.

O [Kid3](https://kid3.kde.org/) tem um novo site com notícias e links para
download de todas as lojas em que ele está disponível.

## Lançamentos 19.12.3

Alguns dos nossos projetos lançam a seu próprio tempo e alguns são lançados
em lotes. O conjunto de projetos do 19.12.3 foi lançado hoje e deve ficar
disponível nas lojas de aplicativos e distribuições em breve. Dê uma olhada
na [página de lançamentos do
19.12.3](https://www.kde.org/info/releases-19.12.3.php). Este conjunto era
chamado anteriormente de KDE Applications mas teve sua nomeação alterada
para se tornar um serviço de lançamentos, deste modo evitando confusão com
todos os outros aplicativos da KDE e também por ser dezenas de produtos
diferentes ao invés de um conjunto só.

Algumas das correções inclusas neste lançamento são:

* O suporte a atributos de arquivos para compartilhamentos SMB foi
  melhorados, [vários
  commits](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)
* O K3b agora aceita arquivos WAV como arquivos de áudio [BUG
  399056](https://bugs.kde.org/show_bug.cgi?id=399056)
  [Commit](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)
* O Gwenview agora pode carregar imagens remotas via HTTPS
  [Commit](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [Notas de lançamento do 19.12 release
notes](https://community.kde.org/Releases/19.12_Release_Notes) para
informações sobre pacotes e problemas conhecidos. 💻 [Página da wiki para
baixar os
pacotes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) 💻
[Página com informações do código-fonte do
19.12.3](https://kde.org/info/releases-19.12.3) 💻  [Registro completo de
alterações do
19.12.3](https://kde.org/announcements/changelog-releases.php?version=19.12.3)
💻
