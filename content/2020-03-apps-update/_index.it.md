---
title: Aggiornamento applicazioni KDE di marzo 2020

publishDate: 2020-03-05 15:50:00 # don't translate

layout: page # don't translate

summary: "Che cosa è successo questo mese alle applicazioni KDE"

type: announcement # don't translate
---

## Nuovi rilasci

### Choqok 1.7

Febbraio è iniziato con l'attesissimo aggiornamento della nostra
applicazione per il microblogging
[Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

Correzioni importanti includono il limite di 280 caratteri in Twitter,
consentendo la disabilitazione degli account e il supporto di tweet estesi.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore e KDE Partition Manager 4.1.0

Partition Manager, il nostro programma per la formattazione del disco, è
stato rilasciato e molto lavoro è stato fatto sulla libreria KPMCore,
utilizzata anche dal programma di installazione della distribuzione
Calamares. La stessa applicazione ha aggiunto il supporto per i file system
Minix.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Partition Manager" >}}

### KPhotoAlbum 5.6

Se possiedi centinaia o migliaia di immagini sul disco, diventa impossibile
ricordare la storia dietro ciascuna immagine, o i nomi delle persone
fotografate. KPhotoAlbum è stato per aiutarci a descrivere le tue immagini e
a ricercare tra le foto in maniera rapida ed efficiente.

Questo rilascio offre in particolare grandi miglioramenti nelle prestazioni
quando etichetti molte immagini e per la vista miniature. Ringraziamo Robert
Krawitz per aver indagato profondamente nel codice, eseguito ottimizzazioni
e rimosso ridondanze!

In aggiunta, questo rilascio aggiunge supporto a Purpose, ll'infrastruttura
per le estensioni di KDE.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## In arrivo

Kid3, l'editor di tag ID MP3, è stato spostato in kdereview, primo passo per
ottenere nuovi rilasci.

E Ruqola, l'applicazione per Rocket.chat, ha passato la fase kdereview, ed è
stata spostata per essere pronta per il rilascio.  Secondo il responsabile
dell'applicazione Laurent Montel, tuttavia, è programmata una riscrittura
del codice con Qt Widgets, vi è dunque un bel po' di lavoro da fare.

## App Store

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Novità nel Microsoft Store per Windows è
[Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

Un lettore musicale moderno e molto bello creato con amore da KDE. Naviga e
riproduci la tua musica in modo facilissimo! Elisa è in grado di leggere
quasi qualsiasi tipo di file audio. Elisa è stato concepito per essere
accessibile a tutti.

Nel frattempo, Digikam è pronto per essere inviato al Microsoft Store.

## Aggiornamenti sito web

[KDE Connect](https://kdeconnect.kde.org/), il nostro agile strumento di
integrazione tra desktop e smartphone, ha un nuovo sito web.

[Kid3](https://kid3.kde.org/) possiede un nuovo e scintillante sito web con
le novità e i collegamenti per lo scaricamento da tutti gli store in cui è
disponibile.

## Rilasci 19.12.3

Alcuni dei nostri progetti vengono rilasciati in modo indipendente, altri
vengono rilasciati in massa. Il gruppo 19.12.3 dei progetti è stato
rilasciato oggi e sarà presto disponibile negli app store nelle
distribuzioni.  Per i dettagli, consulta la [pagina dei rilasci
19.12.3](https://www.kde.org/announcements/releases/19.12.3.php).  Questo
gruppo era chiamato in precedenza KDE Applications ma tale nome è stato
modificato ed è diventato un servizio di rilascio in modo da evitare
confusione con tutte le altre applicazioni create da KDE e in quanto è
composto da decine di prodotti differenti che non vanno considerati come un
singolo prodotto.

Alcune delle correzioni incluse in questo rilascio sono:

* È stato migliorato il supporto degli attributi dei file per le
  condivisioni SMB, [vari
  commit](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)
* K3b ora accetta i file WAV come file audio [BUG
  399056](https://bugs.kde.org/show_bug.cgi?id=399056)
  [Commit](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)
* Gwenview ora può caricare immagini remote tramite https
  [Commit](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [Note di rilascio
19.12](https://community.kde.org/Releases/19.12_Release_Notes) informative
sui tarball e problemi noti. 💻 [Pagina wiki per lo scaricamento dei
pacchetti](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
💻 [Pagina informativa sui sorgenti
19.12.3](https://kde.org/info/releases-19.12.3) 💻 [Elenco completo delle
modifiche
19.12](https://kde.org/announcements/changelog-releases.php?version=19.12.3)
💻
