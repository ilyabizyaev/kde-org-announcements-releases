---
title: Llançaments 19.12 RC

summary: "Uns 120 programes individuals i dotzenes de biblioteques de
programació i connectors de funcionalitats es publiquen simultàniament com a
part del servei de llançaments del KDE."

publishDate: 2019-11-29 00:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

29 de novembre de 2019. Uns 120 programes individuals i dotzenes de
biblioteques de programació i connectors de funcionalitats es publiquen
simultàniament com a part del servei de llançaments del KDE.

Avui tots disposaran del codi font del llançament de la versió candidata,
indicant que han completat les funcionalitats però necessiten proves per
cercar errors.

Cal que els empaquetadors de les distribucions i de les botigues
d'aplicacions actualitzin els seus canals de prellançament per comprovar si
hi ha problemes.

+ [Notes de llançament de la
19.12](https://community.kde.org/Releases/19.12_Release_Notes) per a
informació quan als arxius tar i els problemes coneguts.+ [Pàgina wiki de
baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)  +
[Pàgina d'informació del codi font de la 19.12
RC](https://kde.org/info/applications-19.11.90)

## Contactes de premsa

Per a més informació, envieu-nos un correu:
[press@kde.org](mailto:press@kde.org).
