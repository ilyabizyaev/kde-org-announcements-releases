---
title: Lanzamientos de 19.12 RC

summary: "Alrededor de 120 programas individuales junto con docenas de
bibliotecas de programación y complementos de funcionalidades se publican
simultáneamente como parte del servicio de lanzamientos de KDE."

publishDate: 2019-11-29 00:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

29 de noviembre de 2019. Alrededor de 120 programas individuales junto con
docenas de bibliotecas de programación y complementos de funcionalidades se
publican simultáneamente como parte del servicio de lanzamientos de KDE.

Hoy han alcanzado el estado candidato de lanzamiento, lo que significa que
su funcionalidad está completa a falta de pruebas para corregir los últimos
fallos que pudieran contener.

Los empaquetadores de las distribuciones y de las tiendas de aplicaciones
deben actualizar sus canales de prelanzamiento para comprobar los posibles
problemas.

+ [Notas del lanzamiento de
19.12](https://community.kde.org/Releases/19.12_Release_Notes), para
información sobre «tarballs» y los problemas conocidos.  + [Página wiki de
descarga de
paquetes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [Página fuente de información sobre 19.12
RC](https://kde.org/info/applications-19.11.90)

## Contactos de prensa

Para obtener más información, envíenos un correo electrónico a:
[press@kde.org](mailto:press@kde.org).
