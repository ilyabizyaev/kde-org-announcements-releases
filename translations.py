import os
import subprocess
import shutil
import argparse
import gettext
import copy
import polib

from yaml import safe_load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

def webpage_list():
    """
    Generate a list of webpage that need to be translated
    """
    return [
            'content/19.12-rc/_index.md',
            'content/2019-12-apps-update/_index.md',
            'content/2020-01-apps-update/_index.md',
            'content/2020-02-apps-update/_index.md',
            'content/2020-03-apps-update/_index.md',
            'content/2020-04-apps-update/_index.md',
            ]

def extract(args):
    """
    First parameter will be the path of the pot file we have to create
    """
    pot_file = args.pot
    webpages = ""
    for webpage in webpage_list():
        webpages += " --master \"" + webpage + "\""
    command = "po4a-gettextize --format text --option 'markdown=1' " + webpages + " --master-charset \"UTF-8\" --po \"" + pot_file + "\""
    subprocess.run(command, shell=True, check=True)

    pot = polib.pofile(pot_file, check_for_duplicates=True)

    pot.metadata['Content-Type'] = 'text/plain; charset=UTF-8'

    with open("i18n/en.yaml", 'r') as stream:
        en_string_trans = safe_load(stream)

        for key, string in en_string_trans.items():
            entry = polib.POEntry(
                msgid=string['other'],
                msgstr=u'',
                occurrences=[('i18n/en.yaml', '0')]
            )
            try:
                pot.append(entry)
            except:
                pass

    with open("config.yaml", "r") as stream:
        config = safe_load(stream)

        for menu_item in config['menu']['main']:
            entry = polib.POEntry(
                msgid=menu_item['name'],
                msgstr=u'',
                occurrences=[('config.yaml', '0')]
            )
            try:
                pot.append(entry)
            except:
                pass

        entry = polib.POEntry(
            msgid=config['title'],
            msgstr=u'',
            occurrences=[('config.yaml', '0')]
        )
        try:
            pot.append(entry)
        except:
            pass

    pot.save(pot_file)


def import_po(args):
    """
    First parameter will be a path that will contain several .po files with the format LANG.po
    """
    directory = args.directory
    for translation in os.listdir(directory):
        lang = os.path.splitext(translation)[0]
        if lang == "ca@valencia":
            lang = "ca-valencia"
        if lang == "pt_BR":
            lang = "pt-br"

        os.makedirs("locale/" + lang + "/LC_MESSAGES/")
        po_file = "locale/" + lang + "/LC_MESSAGES/announcement"
        shutil.copyfile(directory + "/" + translation, po_file + ".po")
        command = "msgfmt " + po_file + ".po -o " + po_file + ".mo"
        subprocess.run(command, shell=True, check=True)
        print("Translations files for " + translation + " imported")


def generate_translations(args):
    """
    Assume translation located at `locale/$LANG/LC_MESSAGES/`
    """
    for translations in os.listdir('locale'):
        for webpage in webpage_list():
            extension = os.path.splitext(webpage)[1]
            basename = os.path.splitext(webpage)[0]
            translated = basename + "." + translations + extension
            command = "po4a-translate --format text --option 'markdown=1' --width 99999 --po \"locale/" + translations + "/LC_MESSAGES/announcement.po\" --master \"" + webpage + "\" --localized \"" + translated + "\" --master-charset \"UTF-8\""
            subprocess.run(command, shell=True, check=True)

    with open("i18n/en.yaml", 'r') as stream:
        en_string_trans = safe_load(stream)

    with open("config.yaml", 'r') as config_file:
        config_content = safe_load(config_file)

    for translations in os.listdir('locale'):
        os.environ["LANGUAGE"] = translations
        gettext.bindtextdomain('announcement', os.path.abspath('locale'))
        gettext.textdomain('announcement')
        _ = gettext.gettext


        trans_content = dict()
        for key, string in en_string_trans.items():
            if _(string['other']) is not string['other']:
                trans_content[key] = dict()
                trans_content[key]['other'] = _(string['other'])

        if len(trans_content):
            with open('i18n/' + translations + '.yaml', 'w+') as trans_file:
                trans_file.write(dump(trans_content, default_flow_style=False))

        if not translations in config_content['languages']:
            config_content['languages'][translations] = dict()
            config_content['languages'][translations]['weight'] =  2

        config_content['languages'][translations]['menu'] = dict()
        config_content['languages'][translations]['menu']['main'] = list()

        for menu_item in config_content['menu']['main']:
            menu = copy.deepcopy(menu_item)
            menu['name'] = _(menu['name'])
            config_content['languages'][translations]['menu']['main'].append(menu)

    with open('config.yaml', 'w+') as conf_file:
        conf_file.write(dump(config_content, default_flow_style=False))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help')

    extract_cmd = subparsers.add_parser('extract', help='extract strings for translations')
    extract_cmd.add_argument('pot')
    extract_cmd.set_defaults(func=extract)

    import_po_cmd = subparsers.add_parser('import', help='import translated strings')
    import_po_cmd.add_argument('directory')
    import_po_cmd.set_defaults(func=import_po)

    generate_translations_cmd = subparsers.add_parser('generate-translations', help='generate translated content')
    generate_translations_cmd.set_defaults(func=generate_translations)

    args = parser.parse_args()
    args.func(args)
